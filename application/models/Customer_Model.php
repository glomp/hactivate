<?php
 
class Customer_Model extends CI_Model {
     
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    
    public function saveCustomer ($post_data)
    {
        $data = array(
        	'client_id' => "1", // replace with get client id 
        	'date_created' => date("Y-m-d H:i:s"),
        	'date_updated' => date("Y-m-d H:i:s")
        );
        
        $this->db->insert('h_client_customers', $data); 
        $insert_id = $this->db->insert_id();
        
        if ($this->db->affected_rows() > 0) {
            return $insert_id;
            // return $this->saveCustomerInfo ($insert_id, $post_data);   
        } else {
            return false;
        } 
    }
    
    public function saveCustomerInfo ($customer_id, $post_data)
    {
        foreach ($post_data as $key => $value) {
            $data = array(
                'client_customer_id' => $customer_id,
                'label' => $key,
                'value' => $value,
                'date_created' => date("Y-m-d H:i:s"),
                'date_updated' => date("Y-m-d H:i:s"), 
            );

            $this->db->insert('h_client_customer_infos', $data); 
        } 
        
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
}