<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class IosVoucher extends CI_Controller {
 
    var $requestType;
    var $deviceLibraryIdentifier;
    var $passTypeIdentifier;
    var $serialNumber;
    var $pushToken;
    var $authorizationToken;
    var $version;
    var $url;
    var $vars;
    
    public function __construct()
    {
        parent::__construct();
        $this->load->library('PKPass');
        
        $this->url = uri_string();
     
        $headers = getallheaders(); 
        $this->requestType = $this->input->method(TRUE);
         
        $this->load->model('IosVoucher_Model', '', TRUE);


    }

    private function _delete() {
        if ($this->requestType == "DELETE") {
            // webServiceURL/version/devices/deviceLibraryIdentifier/registrations/passTypeIdentifier/serialNumber
            return $this->IosVoucher_Model->deleteDevice($this->deviceLibraryIdentifier, $this->passTypeIdentifier, $this->serialNumber);
        }
    }

    private function _registration() {
        $data = json_decode(file_get_contents("php://input"));
        $pushtoken = $data->pushToken; //get Push Token

        list($campaign_id, $customer_id) = explode('-', $this->serialNumber);

        $properties = json_encode(
            array(
                'backFields' =>array(),
                'locations' => array()
            )
        );

        $voucher = $this->db->get_where('h_campaign_ios_vouchers', array('serial_number' => $this->serialNumber));

        // save to database  
        $data = array(
            'customer_id' => $customer_id,
            'campaign_id' => $campaign_id,
            'device_id' => $this->deviceLibraryIdentifier,
            'pass_type_id' => $this->passTypeIdentifier,
            'serial_number' =>  $this->serialNumber,
            'push_token' => $pushtoken,
            'version' => 'v1',
            'status' => 'A',
            'properties' => $properties,
            'date_created' => date("Y-m-d H:i:s"),
            'date_updated' => date("Y-m-d H:i:s")
        );

        if ($voucher->num_rows() > 0) {
            $data['date_created'] = $voucher->row()->date_created; //get created date
            return $this->db->where('serial_number', $this->serialNumber)->update('h_campaign_ios_vouchers', $data);
        }

        $this->db->insert('h_campaign_ios_vouchers', $data);
        return;
    }

    private function _requestSerials($campaign_id = 0) {
        $serials = array();
        $where = array(
            'pass_type_id' => $this->passTypeIdentifier,
            'device_id' => $this->deviceLibraryIdentifier,
            'status' => 'A',
        );

        if ($campaign_id != 'v1') {
            $where['campaign_id'] = $campaign_id;
        }

        $vouchers = $this->db->get_where('h_campaign_ios_vouchers', $where);

        foreach ($vouchers->result_array() as $voucher) {
            $serials[] = $voucher['serial_number']; //move all serial numbers to be updated
        }

        
        //$serial_numbers = $this->IosVoucher_Model->getSerialNumbers($this->passTypeIdentifier, $this->deviceLibraryIdentifier);
        $data["lastUpdated"] = (string) time(); 
        $data["serialNumbers"] = $serials;

        return $data;
    }
    
    public function webservice ($campaign_id = 0)
    {
        $url_array = explode("/", $this->url);
        if (strpos($this->url, 'registration') !== FALSE ) {
            $this->deviceLibraryIdentifier = $url_array[4];
            $this->passTypeIdentifier = $url_array[6];

            if (isset($url_array[7])) {
                $this->serialNumber = $url_array[7];
            }
        } else{
            //Download voucher
            $this->passTypeIdentifier = $url_array[4];
            $this->serialNumber = $url_array[5];
        }


        if ($campaign_id != 'v1') {
            //set values from registration, delete and get serials
            if (strpos($this->url, 'registration') !== FALSE ) {
                $this->deviceLibraryIdentifier = $url_array[5];
                $this->passTypeIdentifier = $url_array[7];

                if (isset($url_array[8])) {
                    $this->serialNumber = $url_array[8];
                }
            } else{
                //Download voucher
                $this->passTypeIdentifier = $url_array[5];
                $this->serialNumber = $url_array[6];
            }
        }

        $html = json_encode($url_array) . '<br />';
        $html = 'URL:'. current_url() . '<br />';
        $html .= json_encode($_GET). '<br />';
        $html .= json_encode($_POST). '<br />';
        $html .= json_encode(getallheaders()). '<br />';
        $html .= json_encode(json_decode(file_get_contents("php://input"))). '<br />';
        $myfile = fopen(FCPATH . "test.html", "w") or die("Unable to open file!");
        fwrite($myfile, $html);
        fclose($myfile);

        //exit;


        if ($this->requestType == "POST")  {
            if (strpos($this->url, 'log') !== FALSE ) { 
                $html .= 'Logs: <br />';
                $myfile = fopen(FCPATH . "test.html", "w") or die("Unable to open file!");
                fwrite($myfile, $html);
                fclose($myfile);

                $this->IosVoucher_Model->logs(); // logs
            } else {
                $html .= 'Registration: <br />';
                $myfile = fopen(FCPATH . "test.html", "w") or die("Unable to open file!");
                fwrite($myfile, $html);
                fclose($myfile);

                //continue registration
                return $this->_registration();
            }
        }

        if ($this->requestType == "GET") {
            if (strpos($this->url, 'registration') !== FALSE ) { //get passes to be updated
                $data =  $this->_requestSerials($campaign_id);
                $html .= 'LIST OF SERIALS: <br />';
                $html .= 'DATA: '.json_encode($data).'<br />';
                $myfile = fopen(FCPATH . "test.html", "w") or die("Unable to open file!");
                fwrite($myfile, $html);
                fclose($myfile);

                echo json_encode($data);
                return;
            } else {
                $html .= 'DOWNLOAD: <br />';
                $myfile = fopen(FCPATH . "test.html", "w") or die("Unable to open file!");
                fwrite($myfile, $html);
                fclose($myfile);

                list($campaign_id, $customer_id) = explode('-', $this->serialNumber);
                //Download voucher
                $this->createVoucher($campaign_id, $customer_id, $this->serialNumber);
                return;
            }
        }

        if ($this->requestType == "DELETE") {   
            return $this->_delete(); //delete specific voucher
        }



        //For testing code
        // $url_array = explode("/", $this->url); 
        // $html = json_encode($url_array) . '<br />';
        // $html .= 'get pass from init <br />';
        // $html .= json_encode($_GET). '<br />';
        // $html .= json_encode($_POST). '<br />';
        // $html .= json_encode(getallheaders()). '<br />';
        // $html .= json_encode(json_decode(file_get_contents("php://input"))). '<br />';

        // $myfile = fopen(FCPATH . "test.html", "w") or die("Unable to open file!");
        
        // fwrite($myfile, $html);
        // fclose($myfile);
        // exit;
    
    }

    function message($customer_id = '', $message = 'Engage your customers via push and geo-location notifications.') {

        if (empty($customer_id)) {
            $broadcast = $_POST['broadcast'];
            $message = $_POST['message'];
            $campaign_id = $_POST['campaign_id'];
            $client_ids = $_POST['client_ids'];
        } else {
            $campaign_id = 9999;
            $client_ids = $customer_id;
            $message = urldecode($message);
            $broadcast = 'false';
        }

        $propeties['backFields'][] = array(
            'key'   => 'messages-' . time(),
            'changeMessage' => '%@',
            'label' => 'Messages',
            'value' => $message,
        );

        $data = array(
            'properties' => json_encode($propeties),
            'date_updated' => date("Y-m-d H:i:s")
        );

        if ($broadcast  == 'true') {
            $this->db->where('campaign_id', $campaign_id);
            $this->db->update('h_campaign_ios_vouchers', $data);

            //send push on each voucher
            $vouchers = $this->db->get_where('h_campaign_ios_vouchers', array('campaign_id' => $campaign_id));

            foreach($vouchers->result_array() as $voucher) {
                $this->send($voucher['push_token']);
            }
            return;
        }


        //update vouchers
        $this->db->where('campaign_id = "'.$campaign_id.'" AND customer_id IN ('. $client_ids . ')');
        $this->db->update('h_campaign_ios_vouchers', $data);


        //send push on each voucher
        $this->db->where('campaign_id = "'.$campaign_id.'" AND customer_id IN ('. $client_ids . ')');
        $vouchers = $this->db->get('h_campaign_ios_vouchers');

        foreach($vouchers->result_array() as $voucher) {
            $this->send($voucher['push_token']);
        }

        return;
    }

    function locator($campaign_id = 0, $id = 1) {

        $locations[1] = array(
            [
                'latitude'   => 1.3506385,
                'longitude' => 103.8717633,
                'relevantText' => 'The Body Shop NEX',
            ],
            // [ 
            //     'latitude'   => 14.514244148826718,
            //     'longitude' => 121.04530334472656,
            //     'relevantText' => 'BLK 18 LOT 8 NEARBY 1',
            // ], 
            [ 
                'latitude'   => 1.304052,
                'longitude' => 103.831767,
                'relevantText' => 'The Body Shop ION Orchard',
            ],
            [ 
                'latitude'   => 1.303763,
                'longitude' => 103.835499,
                'relevantText' => 'The Body Shop Paragon',
            ],
            [ 
                'latitude'   => 1.301016,
                'longitude' => 103.845411,
                'relevantText' => 'The Body Shop Plaza Singapura',
            ],
            [ 
                'latitude'   => 1.304713,
                'longitude' => 103.823345,
                'relevantText' => 'The Body Shop Tanglin Mall',
            ],
            [ 
                'latitude'   => 1.2863897,
                'longitude' => 103.8271385,
                'relevantText' => 'The Body Shop Tiong Bahru Plaza',
            ],
            [ 
                'latitude'   => 1.2931317,
                'longitude' => 103.8514392,
                'relevantText' => 'The Body Shop Capitol Piazza',
            ],
            [ 
                'latitude'   => 1.2936854,
                'longitude' => 103.8572793,
                'relevantText' => 'The Body Shop Suntec City Mall',
            ],
            [ 
                'latitude'   => 1.2636446,
                'longitude' => 103.8216888,
                'relevantText' => 'The Body Shop VivoCity',
            ],
            [ 
                'latitude'   => 1.2934852,
                'longitude' => 103.8531848,
                'relevantText' => 'The Body Shop Raffles City',
            ],
        );

        $locations[2] = array(
            [
                'latitude'   => 1.2989548,
                'longitude' => 103.8552775,
                'relevantText' => 'The Body Shop Bugis Junction',
            ], 
            [ 
                'latitude'   => 1.293984,
                'longitude' => 103.832088,
                'relevantText' => 'The Body Shop Great World City',
            ],
            [ 
                'latitude'   => 1.2852885,
                'longitude' => 103.8450511,
                'relevantText' => 'The Body Shop Chinatown Point',
            ],
            // [ 
            //     'latitude'   => 14.514244148826718,
            //     'longitude' => 121.04530334472656,
            //     'relevantText' => 'The Body Shop Chinatown Point Nearby',
            // ],
            [ 
                'latitude'   => 1.320278,
                'longitude' => 103.8437595,
                'relevantText' => 'The Body Shop Novena Square',
            ],
            [ 
                'latitude'   => 1.349971,
                'longitude' => 103.848794,
                'relevantText' => 'The Body Shop Junction 8',
            ],
            [ 
                'latitude'   => 1.331827,
                'longitude' => 103.847565,
                'relevantText' => 'The Body Shop HDB Hub',
            ],
            [ 
                'latitude'   => 1.3111802,
                'longitude' => 103.8568038,
                'relevantText' => 'The Body Shop City Square Mall',
            ],
            [ 
                'latitude'   => 1.4061464,
                'longitude' => 103.9018679,
                'relevantText' => 'The Body Shop Waterway Point',
            ],
            [ 
                'latitude'   => 1.3691264,
                'longitude' => 103.8489935,
                'relevantText' => 'The Body Shop AMK Hub',
            ],
            [ 
                'latitude'   => 1.3727928,
                'longitude' => 103.8939741,
                'relevantText' => 'The Body Shop Hougang Mall',
            ],
        );  

        $locations[3] = array(
            [
                'latitude'   => 1.429848,
                'longitude' => 103.835554,
                'relevantText' => 'The Body Shop Northpoint',
            ], 
            [ 
                'latitude'   => 1.435855,
                'longitude' => 103.786222,
                'relevantText' => 'The Body Shop Causeway Point',
            ],
            [ 
                'latitude'   => 1.3526453,
                'longitude' => 103.9448867,
                'relevantText' => 'The Body Shop Tampines Mall',
            ],
            [ 
                'latitude'   => 1.324877,
                'longitude' => 103.9292199,
                'relevantText' => 'The Body Shop Bedok Mall',
            ],
            [ 
                'latitude'   => 1.3019864,
                'longitude' => 103.9047241,
                'relevantText' => 'The Body Shop Parkway Parade',
            ],
            [ 
                'latitude'   => 1.3360557,
                'longitude' => 103.9639813,
                'relevantText' => 'The Body Shop Changi City Point',
            ],
            [ 
                'latitude'   => 1.356716,
                'longitude' => 103.98651,
                'relevantText' => 'The Body Shop Changi Airport Terminal 3',
            ],
            [ 
                'latitude'   => 1.311791,
                'longitude' => 103.795452,
                'relevantText' => 'The Body Shop Holland Village',
            ],
            [ 
                'latitude'   => 1.333144,
                'longitude' => 103.743577,
                'relevantText' => 'The Body Shop JEM',
            ],
            [ 
                'latitude'   => 1.3400385,
                'longitude' => 103.7066346,
                'relevantText' => 'The Body Shop Jurong Point',
            ],
        );

        $locations[4] = array(
            [
                'latitude'   => 1.3148677,
                'longitude' => 103.7640477,
                'relevantText' => 'The Body Shop The Clementi Mall',
            ], 
            [ 
                'latitude'   => 1.384848,
                'longitude' => 103.745046,
                'relevantText' => 'The Body Shop LOT 1',
            ],
            [ 
                'latitude'   => 1.379962,
                'longitude' => 103.7641529,
                'relevantText' => 'The Body Shop Bukit Panjang Plaza',
            ],
            [ 
                'latitude'   => 1.3187079,
                'longitude' => 103.8937623,
                'relevantText' => 'The Body Shop SINGPOST CENTRE',
            ],
            [ 
                'latitude'   => 1.392272,
                'longitude' => 103.894775,
                'relevantText' => 'The Body Shop COMPASS ONE',
            ],
            [ 
                'latitude'   => 14.514244148826718,
                'longitude' => 121.04530334472656,
                'relevantText' => 'BLK 18 LOT 8 NEARBY',
            ],
        );


        $propeties['locations'] = $locations[$id];

        $data = array(
            'properties' => json_encode($propeties),
            'date_updated' => date("Y-m-d H:i:s")
        );

        $this->db->where('campaign_id', $campaign_id);
        $this->db->where('status', 'A');
        $this->db->update('h_campaign_ios_vouchers', $data);

        //send push to every record on the database

        //send push on each voucher
        $vouchers = $this->db->get_where('h_campaign_ios_vouchers', array(
            'campaign_id' => $campaign_id,
            'status' => 'A'
        ));

        foreach($vouchers->result_array() as $voucher) {
            $this->send($voucher['push_token']);
        }

        //testing
        $myfile = fopen(FCPATH . "test.html", "a") or die("Unable to open file!");
        fwrite($myfile, 'BATCH '.$id .': '.date('M-d-y h:i:s a'). '/n/r');
        fclose($myfile);

        // */3 * * * * /usr/bin/curl https://staging2.glomp.it/hactivate_live/iosVoucher/locator/3/
        // */3 * * * * /bin/sleep 30;/usr/bin/curl https://staging2.glomp.it/hactivate_live/iosVoucher/locator/3/2
        // */3 * * * * /bin/sleep 60;/usr/bin/curl https://staging2.glomp.it/hactivate_live/iosVoucher/locator/3/3
        // */3 * * * * /bin/sleep 90;/usr/bin/curl https://staging2.glomp.it/hactivate_live/iosVoucher/locator/3/4
        return;

    }
    
    function send($push_token) 
    {
    
        $pass_identifier = "pass.com.hactvat8";
        $ctx = stream_context_create();
        stream_context_set_option($ctx, 'ssl', 'local_cert', FCPATH . "assets/certificates/hactivatePassId.pem");
        stream_context_set_option($ctx, 'ssl', 'passphrase', '');

        // Open a connection to the APNS server
        $fp = stream_socket_client(
                "ssl://gateway.push.apple.com:2195", $err,
                $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

        if (!$fp)
                exit("Failed to connect: $err $errstr" . PHP_EOL);

        //echo 'Connected to APNS' . PHP_EOL;

        // Create the payload body
        $body['aps'] = "";
        
        // Encode the payload as JSON
        $payload = '{}'; 

        // Build the binary notification
        $msg = chr(0) . pack('n', 32) . pack('H*', $push_token) . pack('n', strlen($payload)) . $payload . pack('n', strlen($pass_identifier)) . $pass_identifier;

        // Send it to the server
        $result = fwrite($fp, $msg, strlen($msg));

        if (!$result)
                echo "false";
        else
                echo "Sent!";
        // Close the connection to the server
        fclose($fp);
    }
    
    public function test()
    {
        $voucher_data = $this->IosVoucher_Model->getVoucherJSON($this->input->get('campaign', true));
        print_r($voucher_data['style_keys']); 
         
    }

    public function checkVoucherLimit($campaign_id = 0) {

        header('Content-Type: text/event-stream');
        header('Cache-Control: no-cache');


        $this->db->select('COUNT(*) AS total');
        $this->db->like('logs', '"action":"download","page":"landing\/ios"');
        $vouchers = $this->db->get_where('h_campaign_log', array('campaign_id' => $campaign_id));


        $this->db->select('COUNT(*) AS total');
        $this->db->like('logs', '"action":"download","page":"landing\/android"');
        $vouchers_a = $this->db->get_where('h_campaign_log', array('campaign_id' => $campaign_id));


        $ios = $vouchers->row()->total;
        $android = $vouchers_a->row()->total;

        $startedAt = time();

        do {
          // Cap connections at 10 seconds. The browser will reopen the connection on close
          if ((time() - $startedAt) > 10) {
            die();
          }
            //echo "id: $id" . PHP_EOL;
            echo "data: {\n";
            echo "data: \"ios\": $ios, \n";
            echo "data: \"android\": $android\n";
            echo "data: }\n";
            echo PHP_EOL;
            ob_flush();
            flush();
          sleep(5);
          // If we didn't use a while loop, the browser would essentially do polling
          // every ~3seconds. Using the while, we keep the connection open and only make
          // one request.
        } while(true);

    }
    
    public function createVoucher($campaign_id, $client_id, $serial_number = "")
    {
        $pass = new PKPass();

        $pass->setCertificate('assets/certificates/hactivatePassId.p12');  // 1. Set the path to your Pass Certificate (.p12 file)
        //$pass->setCertificatePassword('P@$$w0rd123');     // 2. Set password for certificate
        //$pass->setCertificatePassword('glompappV1');     // 2. Set password for certificate
        $pass->setWWDRcertPath('assets/certificates/AWWDR.pem'); // 3. Set the path to your WWDR Intermediate certificate (.pem file)
        $serial = "";
        $client = "";

        if (strlen($serial_number) > 0) {
            $serial_array = explode("-",$serial_number);
            $serial = $serial_number;
            $client = $serial_array[1];
        } else {
            $serial = $campaign_id . "-" . $client_id;
            $client = $client_id;
        }

        if ($campaign_id == 1 ) {
            // PANG MARTELL
        
            // // Top-Level Keys http://developer.apple.com/library/ios/#documentation/userexperience/Reference/PassKit_Bundle/Chapters/TopLevel.html
            $standardKeys         = [ 
                'webServiceURL'      => 'https://119.9.88.193/IosVoucher/webservice/',
                'passTypeIdentifier' => 'pass.com.hactvat8',
                'teamIdentifier'     => 'UD4F7T7J39',
                'description'        => "马爹利MARTELL",
                'formatVersion'      => 1,
                'organizationName'   => "马爹利MARTELL", 
                'serialNumber'       => $serial,
                "authenticationToken" => "PZ8GL23T24JEB41X6U"
                           // 4. Set to yours
            ];
             
            
            $associatedAppKeys    = [];
            $relevanceKeys        = [
                'maxDistance' => 200,
                'locations' => [
                     [ 
                        'latitude'   => 1.362433,
                        'longitude' => 103.990333,
                        'relevantText' => '80 Airport Blvd, Singapore 819642',
                    ],  
                    [ 
                        'latitude'   => 1.3556,
                        'longitude' => 103.989717,
                        'relevantText' => '60 Airport Blvd, Singapore 819643',
                    ],
                    [ 
                        'latitude'   => 1.356617,
                        'longitude' => 103.98605,
                        'relevantText' => '65 Airport Blvd, Singapore 819663',
                    ], 
                    [
                        'latitude'   => 14.514249,
                        'longitude' => 121.045123,
                        'relevantText' => 'Hey Allan',
                    ],
                    [
                        'latitude'   => 14.51429,
                        'longitude' => 121.04525,
                        'relevantText' => 'Hey Allan 2',
                    ],

                ],
            ];

            // $styleKeys            = $voucher_data['style_keys']; 
           
            $styleKeys  = [
                'generic' => [
                    'primaryFields'   => [
                        [
                            'key'   => 'origin',
                            'label' => '新品上市独家礼赠',
                            'value' => '5月1 日 – 6月30日 就在新加坡樟机场',
                        ] 
                    ],
                    'secondaryFields' => [
                        [
                            'key'   => 'value1',
                            'label' => '请于领取礼品时向DFS免税店内职员出示此礼券并于右下方',
                            'value' => '',
                        ], 
                         
                    ],
                    'auxiliaryFields' => [
                        [
                            'key'   => 'value1',
                            'label' => '按“i”换取礼品, 万勿错过!',
                            'value' => '',
                        ],
                    ],
                    'backFields'      => [
                        [
                            'key'   => 'passenger-name',
                            'label' => 'Info',
                            'value' => '<a href="' . base_url('landing/page/martell/redeem/confirm/ios/' . $client) . '">换取</a>' ,
                        ],
                    ]
                ],
            ];



            //$visualAppearanceKeys = $voucher_data['visual_appearance_keys'];
            
            $visualAppearanceKeys = [
                'foregroundColor' => 'rgb(242,207,147)',
                'backgroundColor' => 'rgb(18,20,66)',
                'labelColor' => 'rgb(255,255,255)',
                'logoText'        => '马爹利MARTELL',
            ];
     
        
        } //end martell

        if ($campaign_id == 9999 ) {
            // PANG DEMO
        
            // // Top-Level Keys http://developer.apple.com/library/ios/#documentation/userexperience/Reference/PassKit_Bundle/Chapters/TopLevel.html
            $standardKeys         = [ 
                'webServiceURL'      => 'https://staging2.glomp.it/hactivate/IosVoucher/webservice/',
                'passTypeIdentifier' => 'pass.com.hactvat8',
                'teamIdentifier'     => 'UD4F7T7J39',
                'description'        => "h.@ctiv8",
                'formatVersion'      => 1,
                'organizationName'   => "h.@ctiv8", 
                'serialNumber'       => $serial,
                "authenticationToken" => "PZ8GL23T24JEB41X6U"
                           // 4. Set to yours
            ];
             
            
            $associatedAppKeys    = [];
            $relevanceKeys        = [
                'maxDistance' => 1000,
                'locations' => [ 
                    [
                        'latitude'   => 22.2867446,
                        'longitude' => 114.2127653,
                        'relevantText' => '979 Kings Rd, Quarry Bay, Hong Kong',
                    ], 
                    [ 
                        'latitude'   => 22.3122493,
                        'longitude' => 114.2232143,
                        'relevantText' => '100 How Ming St, Kwun Tong',
                    ],
                    [ 
                        'latitude'   => 22.2798226,
                        'longitude' => 114.18379720000007,
                        'relevantText' => '500 Hennessy Rd, Causeway Bay',
                    ],
                    [ 
                        'latitude'   => 22.2590004,
                        'longitude' => 114.13211449999994,
                        'relevantText' => 'Central Cyberport 3, 100 Cyberport Rd, Telegraph Bay',
                    ],
                    [ 
                        'latitude'   => 22.284609,
                        'longitude' => 114.15473999999995,
                        'relevantText' => '99 Queen\'s Road Central',
                    ],
                    [ 
                        'latitude'   => 1.2743083,
                        'longitude' => 103.84566990000008,
                        'relevantText' => '77 Robinson Rd',
                    ],
                    [
                        'latitude'   =>1.2840999,
                        'longitude' => 103.8513269,
                        'relevantText' => 'Raffles Place, Downtown Core, Singapore',
                    ],
                    [ 
                        'latitude'   => 1.3010401,
                        'longitude' => 103.83838220000007,
                        'relevantText' => '313 Orchard Rd, Singapore 238895',
                    ],
                    [ 
                        'latitude'   => 1.3109027,
                        'longitude' => 103.83712029999992,
                        'relevantText' => '50 Scotts Rd',
                    ],
                    [ 
                        'latitude'   => 1.2892255,
                        'longitude' => 103.84426770000005,
                        'relevantText' => '30 Merchant Rd, Riverside Point',
                    ]

                ],
            ];

            // $styleKeys            = $voucher_data['style_keys']; 
           
            $styleKeys  = [
                'eventTicket' => [
                    'primaryFields'   => [
                        [
                            'key'   => 'origin',
                            'label' => 'Welcome to h.@ctiv8',
                            'value' => 'A true O2O mobile solution',
                        ] 
                    ],
                    'secondaryFields' => [
                        [
                            'key'   => 'value1',
                            'label' => 'featuring transparent tracking',
                            'value' => 'push and geolocation reminders.',
                        ], 
                         
                    ],
                    'auxiliaryFields' => [
                        [
                            'key'   => 'value1',
                            'label' => 'Please click \'i\' below to redeem',
                            'value' => '',
                        ],
                    ],
                    'backFields'      => [
                        [
                            'key'   => 'passenger-name',
                            'label' => 'Info',
                            'value' => '<a href="' . base_url('landing/page/demo/redeem/confirm/ios/' . $client) . '">Redeem</a>' ,
                        ],
                    ]
                ],
            ];

            $voucher = $this->db->get_where('h_campaign_ios_vouchers', array(
                'customer_id' => $client_id,
                'campaign_id' => $campaign_id
            ));

            if ($voucher->num_rows() > 0) {
                $property = json_decode($voucher->row()->properties);
                // echo '<pre>';
                // print_r($property); 

                //change please
                if (count($property->backFields) >0 ) {
                    $styleKeys['eventTicket']['backFields'][] = (array) $property->backFields[0]; //add new message   
                }   
            }

            // echo '<pre>';
            // print_r($styleKeys);
            // exit;


            //$visualAppearanceKeys = $voucher_data['visual_appearance_keys'];
            
            $visualAppearanceKeys = [
                'foregroundColor' => 'rgb(255,255,255)',
                'backgroundColor' => 'rgb(80,81,83)',
                'labelColor' => 'rgb(255,255,255)',
                'logoText'        => 'h.@ctiv8',
            ];
     
        
        } //end demo
        
        if ($campaign_id == 2) {
            // // Top-Level Keys http://developer.apple.com/library/ios/#documentation/userexperience/Reference/PassKit_Bundle/Chapters/TopLevel.html
            $standardKeys         = [ 
                'webServiceURL'      => 'https://119.9.88.193/IosVoucher/webservice/',
                'passTypeIdentifier' => 'pass.com.hactvat8',
                'teamIdentifier'     => 'UD4F7T7J39',
                'description'        => "Issey Miyake",
                'formatVersion'      => 1,
                'organizationName'   => "Issey Miyake", 
                'serialNumber'       => $serial,
                "authenticationToken" => "PZ8GL23T24JEB41X6U"
                           // 4. Set to yours
            ];
             
            
            $associatedAppKeys    = [];
            $relevanceKeys        = [
                'maxDistance' => 200,
                'locations' => [
                    [ 
                        'latitude'   => 1.302962,
                        'longitude' => 103.837189,
                        'relevantText' => 'Store nearby on The Hereen',
                    ], 
                    [ 
                        'latitude'   => 22.2811499,
                        'longitude' => 114.15260940000007,
                        'relevantText' => 'No internet test',
                    ],   
                    [
                        'latitude'   => 14.5614206,
                        'longitude' => 121.01424700000007,
                        'relevantText' => 'Hey allan testing',
                    ],
                    [
                        'latitude'   => 22.3901185,
                        'longitude' => 114.2092782,
                        'relevantText' => 'Delta House, 3 On Yiu Street',
                    ]
                    
                ],
            ];

            // $styleKeys            = $voucher_data['style_keys']; 
           
            $styleKeys  = [
                'generic' => [
                    'primaryFields'   => [
                        [
                            'key'   => 'origin',
                            'label' => 'Experience a suspended moment with',
                            'value' => 'L\'eau d\'Issey Pure eau de toilette',
                        ] 
                    ],
                    'secondaryFields' => [
                        [
                            'key'   => 'value1',
                            'label' => '',
                            'value' => 'Present this e-voucher to collect your sample from the nearest Issey Miyake counter.',
                        ], 
                        /**
                        [
                            'key'   => 'date',
                            'label' => 'Secondary Field 2',
                            'value' => 'http://google.com',
                        ],
                        **/

                    ],
                    'auxiliaryFields' => [
                        [
                            'key'   => 'value1',
                            'label' => '',
                            'value' => 'Whilst stocks last! Click the “i” button to redeem.                               ',
                        ],
                        
                    ],
                    'backFields'      => [
                        [
                            'key'   => 'passenger-name',
                            'label' => 'Info',
                            'value' => '<a style="font-size:25px" href="' . base_url('landing/page/issey-miyake/redeem/confirm/ios/' . $client) . '">Redeem</a>' ,
                        ],
                    ]
                ],
            ];

            //$visualAppearanceKeys = $voucher_data['visual_appearance_keys'];
            
            $visualAppearanceKeys = [
                'foregroundColor' => 'rgb(0,0,0)',
                'backgroundColor' => 'rgb(241, 245, 243)',
                'labelColor' => 'rgb(0,0,0)',
                'logoText'        => 'Issey Miyake',
            ];
        }


        if ($campaign_id == 3 ) {
            // bodyshop

            $domain_names = array(
                'duckndive.info' => '',
                'www.duckndive.info' => '',  
                '192.168.0.10' => '',
                'staging2.glomp.it' => '', //need this
            );


            $url_array = explode("/", base_url());
            $from_hactivate_live = isset($url_array[3]) ? $url_array[3] : '';
            // // Top-Level Keys http://developer.apple.com/library/ios/#documentation/userexperience/Reference/PassKit_Bundle/Chapters/TopLevel.html

            if ( isset($domain_names[$_SERVER['HTTP_HOST']]) == true) { //if domain listed
                //for testing
                $webServiceURL = 'https://staging2.glomp.it/hactivate/IosVoucher/webservice/'.$campaign_id.'/';
                $redeem_url = base_url('landing/page/bodyshop/redeem/confirm/ios/' . $client);

                //from live
                if ($from_hactivate_live == 'hactivate_live' || $_SERVER['HTTP_HOST'] == 'duckndive.info' ||  $_SERVER['HTTP_HOST']  == 'www.duckndive.info' ) { //if from staging2.glomp.it/hactivate_live or duckndive.info
                    $webServiceURL = 'https://staging2.glomp.it/hactivate_live/IosVoucher/webservice/'.$campaign_id.'/'; //support SSL workaround
                    $redeem_url = 'http://duckndive.info/landing/page/bodyshop/redeem/confirm/ios/' . $client; //since webservice will download the new voucher
                }

            } else {
                //from local?
                $webServiceURL = base_url('IosVoucher/webservice/'.$campaign_id.'/');
                $redeem_url = base_url('landing/page/bodyshop/redeem/confirm/ios/' . $client);
            }

            // echo $webServiceURL;
            // exit;


            $standardKeys         = [ 
                'webServiceURL'      => $webServiceURL,
//                'webServiceURL'      => 'https://192.168.0.20/hactivate/IosVoucher/webservice/'.$campaign_id.'/',
                'passTypeIdentifier' => 'pass.com.hactvat8',
                'teamIdentifier'     => 'UD4F7T7J39',
                'description'        => "The Body Shop Singapore",
                'formatVersion'      => 1,
                'organizationName'   => "The Body Shop Singapore", 
                'serialNumber'       => $serial,
                "authenticationToken" => "PZ8GL23T24JEB41X6U"
                           // 4. Set to yours
            ];

            $associatedAppKeys    = [];

            $relevanceKeys        = [
                'description' => 'The Body Shop Singapore offer',
                'maxDistance' => 500,
                'locations' => [ 
                    [
                        'latitude'   => 1.3506385,
                        'longitude' => 103.8717633,
                        'relevantText' => 'The Body Shop NEX',
                    ], 
                    [ 
                        'latitude'   => 1.304052,
                        'longitude' => 103.831767,
                        'relevantText' => 'The Body Shop ION Orchard',
                    ],
                    [ 
                        'latitude'   => 1.303763,
                        'longitude' => 103.835499,
                        'relevantText' => 'The Body Shop Paragon',
                    ],
                    [ 
                        'latitude'   => 1.301016,
                        'longitude' => 103.845411,
                        'relevantText' => 'The Body Shop Plaza Singapura',
                    ],
                    [ 
                        'latitude'   => 1.304713,
                        'longitude' => 103.823345,
                        'relevantText' => 'The Body Shop Tanglin Mall',
                    ],
                    [ 
                        'latitude'   => 1.2863897,
                        'longitude' => 103.8271385,
                        'relevantText' => 'The Body Shop Tiong Bahru Plaza',
                    ],
                    [ 
                        'latitude'   => 1.2931317,
                        'longitude' => 103.8514392,
                        'relevantText' => 'The Body Shop Capitol Piazza',
                    ],
                    [ 
                        'latitude'   => 1.2936854,
                        'longitude' => 103.8572793,
                        'relevantText' => 'The Body Shop Suntec City Mall',
                    ],
                    [ 
                        'latitude'   => 1.2636446,
                        'longitude' => 103.8216888,
                        'relevantText' => 'The Body Shop VivoCity',
                    ],
                    [ 
                        'latitude'   => 1.2934852,
                        'longitude' => 103.8531848,
                        'relevantText' => 'The Body Shop Raffles City',
                    ],
                ],
            ];

            // $styleKeys            = $voucher_data['style_keys']; 
           
            $styleKeys  = [
                'storeCard' => [
                    'headerFields' => [
                        [
                            'key' => '',
                            'value' => 'The Body Shop Singapore',
                            'label' => 'Store',
                            'changeMessage' => 'The store name has been updated.',
                            'textAlignment' => 'PKTextAlignmentRight',
                        ]
                    ],
                    'secondaryFields' => [
                        [
                            'key' => 'item',
                            'label' => 'ITEM',
                            'value' => 'Duck n\' Dive box',
                        ],
                        [
                            'key' => 'special-price',
                            'label' => 'SPECIAL PRICE',
                            'value' => '$10 (U.P. $22)',
                            'textAlignment' => 'PKTextAlignmentRight',
                        ]
                    ],                    
                    'backFields' => [
                        [
                            'key'   => 'description',
                            'label' => '',
                            'value' => 'The Body Shop Singapore
                            
Present this voucher in stores and redeem our Christmas Limited Edition Duck n’ Dive gift set at just $10 (U.P. $22)!

Voucher is valid till 25 Dec 2017 at all The Body Shop stores except IMM and Takashimaya Basement 1.
Voucher is not valid with any other promotions. Voucher is limited to 1 redemption per customer.
While stocks last.

Do not click \'Redeem\' until you\'re at the store presenting this voucher to the store staff',
                        ],
                        [
                            'key'   => 'redeem',
                            'label' => 'Info',
                            'value' => '<a href="' . $redeem_url . '">Redeem</a>' ,
                        ],
                    ],
                ],
            ];

            $voucher = $this->db->get_where('h_campaign_ios_vouchers', array(
                'customer_id' => $client_id,
                'campaign_id' => $campaign_id
            ));

            if ($voucher->num_rows() > 0) {
                $property = json_decode($voucher->row()->properties);
                // echo '<pre>';
                // print_r($property); 

                //change please
                if (isset($property->backFields) && count($property->backFields) >0 ) {
                    $styleKeys['storeCard']['backFields'][] = (array) $property->backFields[0]; //add new message   
                } 

                $property = json_decode($voucher->row()->properties, TRUE);

                if (isset($property['locations']) && count($property['locations']) > 0 ) {
                    $relevanceKeys['locations'] = $property['locations']; //clear and add new locations here
                }
            }

            // echo '<pre>';
            // print_r($relevanceKeys);
            // exit;


            //$visualAppearanceKeys = $voucher_data['visual_appearance_keys'];
            
            $visualAppearanceKeys = [
                'foregroundColor' => 'rgb(250, 250, 250)',
                'backgroundColor' => 'rgb(0, 88, 75)',
                'labelColor' => 'rgb(250, 250, 250)',
                'logoText' => '',
            ];            
     
        
        } //end bodyshop A


        if ($campaign_id == 4 ) {
            // bodyshop

            $domain_names = array(
                'truthdare.info' => '',
                'www.truthdare.info' => '', 
                '192.168.0.12' => '',
                'staging2.glomp.it' => '', //need this
            );

            $url_array = explode("/", base_url());
            $from_hactivate_live = isset($url_array[3]) ? $url_array[3] : '';
            // // Top-Level Keys http://developer.apple.com/library/ios/#documentation/userexperience/Reference/PassKit_Bundle/Chapters/TopLevel.html

            if ( isset($domain_names[$_SERVER['HTTP_HOST']]) == true) { //if domain listed
                //for testing
                $webServiceURL = 'https://staging2.glomp.it/hactivate/IosVoucher/webservice/'.$campaign_id.'/';
                $redeem_url = base_url('landing/page/bodyshop-b/redeem/confirm/ios/' . $client);

                //from live
               if ($from_hactivate_live == 'hactivate_live' || $_SERVER['HTTP_HOST'] == 'truthdare.info' || $_SERVER['HTTP_HOST'] == 'www.truthdare.info' ) { //if from staging2.glomp.it/hactivate_live or truthdare.info
                    $webServiceURL = 'https://staging2.glomp.it/hactivate_live/IosVoucher/webservice/'.$campaign_id.'/'; //support SSL workaround
                    $redeem_url = 'http://truthdare.info/landing/page/bodyshop-b/redeem/confirm/ios/' . $client; //since webservice will download the new voucher
                }

            } else {
                //from local?
                $webServiceURL = base_url('IosVoucher/webservice/'.$campaign_id.'/');
                $redeem_url = base_url('landing/page/bodyshop-b/redeem/confirm/ios/' . $client);
            }

            $standardKeys         = [ 
                'webServiceURL'      => $webServiceURL,
//                'webServiceURL'      => 'https://192.168.1.107/hactivate/IosVoucher/webservice/'.$campaign_id.'/',
                'passTypeIdentifier' => 'pass.com.hactvat8',
                'teamIdentifier'     => 'UD4F7T7J39',
                'description'        => "The Body Shop Singapore",
                'formatVersion'      => 1,
                'organizationName'   => "The Body Shop Singapore", 
                'serialNumber'       => $serial,
                "authenticationToken" => "PZ8GL23T24JEB41X6U"
                           // 4. Set to yours
            ];

            $associatedAppKeys    = [];

            $relevanceKeys        = [
                'description' => 'The Body Shop Singapore offer',
                'maxDistance' => 500,
                'locations' => [ 
                    [
                        'latitude'   => 1.3506385,
                        'longitude' => 103.8717633,
                        'relevantText' => 'The Body Shop NEX',
                    ], 
                    [ 
                        'latitude'   => 1.304052,
                        'longitude' => 103.831767,
                        'relevantText' => 'The Body Shop ION Orchard',
                    ],
                    [ 
                        'latitude'   => 1.303763,
                        'longitude' => 103.835499,
                        'relevantText' => 'The Body Shop Paragon',
                    ],
                    [ 
                        'latitude'   => 1.301016,
                        'longitude' => 103.845411,
                        'relevantText' => 'The Body Shop Plaza Singapura',
                    ],
                    [ 
                        'latitude'   => 1.304713,
                        'longitude' => 103.823345,
                        'relevantText' => 'The Body Shop Tanglin Mall',
                    ],
                    [ 
                        'latitude'   => 1.2863897,
                        'longitude' => 103.8271385,
                        'relevantText' => 'The Body Shop Tiong Bahru Plaza',
                    ],
                    [ 
                        'latitude'   => 1.2931317,
                        'longitude' => 103.8514392,
                        'relevantText' => 'The Body Shop Capitol Piazza',
                    ],
                    [ 
                        'latitude'   => 1.2936854,
                        'longitude' => 103.8572793,
                        'relevantText' => 'The Body Shop Suntec City Mall',
                    ],
                    [ 
                        'latitude'   => 1.2636446,
                        'longitude' => 103.8216888,
                        'relevantText' => 'The Body Shop VivoCity',
                    ],
                    [ 
                        'latitude'   => 1.2934852,
                        'longitude' => 103.8531848,
                        'relevantText' => 'The Body Shop Raffles City',
                    ],
                ],
            ];

            // $styleKeys            = $voucher_data['style_keys']; 
           
            $styleKeys  = [
                'storeCard' => [
                    'headerFields' => [
                        [
                            'key' => '',
                            'value' => 'The Body Shop Singapore',
                            'label' => 'Store',
                            'changeMessage' => 'The store name has been updated.',
                            'textAlignment' => 'PKTextAlignmentRight',
                        ]
                    ],
                    'secondaryFields' => [
                        [
                            'key' => 'item',
                            'label' => 'ITEM',
                            'value' => 'Truth or Dare box',
                        ],
                        [
                            'key' => 'special-price',
                            'label' => 'SPECIAL PRICE',
                            'value' => '$10 (U.P. $20)',
                            'textAlignment' => 'PKTextAlignmentRight',
                        ]
                    ],                    
                    'backFields' => [
                        [
                            'key'   => 'description',
                            'label' => '',
                            'value' => 'The Body Shop Singapore

Present this voucher in stores and redeem our Christmas Limited Edition Truth Or Dare gift set at just $10 (U.P. $20)!

Voucher is valid till 25 Dec 2017 at all The Body Shop stores except IMM and Takashimaya Basement 1.
Voucher is not valid with any other promotions. Voucher is limited to 1 redemption per customer.
While stocks last.

Do not click ‘Redeem’ until you’re at the store presenting this voucher to the store staff.',
                        ],
                        [
                            'key'   => 'redeem',
                            'label' => 'Info',
                            'value' => '<a href="' . $redeem_url . '">Redeem</a>' ,
                        ],
                    ],
                ],
            ];

            $voucher = $this->db->get_where('h_campaign_ios_vouchers', array(
                'customer_id' => $client_id,
                'campaign_id' => $campaign_id
            ));

            if ($voucher->num_rows() > 0) {
                $property = json_decode($voucher->row()->properties);
                // echo '<pre>';
                // print_r($property); 

                //change please
                if (isset($property->backFields) && count($property->backFields) >0 ) {
                    $styleKeys['generic']['backFields'][] = (array) $property->backFields[0]; //add new message   
                } 

                $property = json_decode($voucher->row()->properties, TRUE);

                if (isset($property['locations']) && count($property['locations']) > 0 ) {
                    $relevanceKeys['locations'] = $property['locations']; //clear and add new locations here
                }
            }

            // echo '<pre>';
            // print_r($relevanceKeys);
            // exit;


            //$visualAppearanceKeys = $voucher_data['visual_appearance_keys'];
            
            $visualAppearanceKeys = [
                'foregroundColor' => 'rgb(250, 250, 250)',
                'backgroundColor' => 'rgb(0, 88, 75)',
                'labelColor' => 'rgb(250, 250, 250)',
                'logoText' => '',
            ];            
     
        
        } //end bodyshop B

        if ($campaign_id == 5 ) {
        // carlsberg

            $domain_names = array(
                'beerwithfood.me' => '',
                'www.beerwithfood.me' => '', 
                '192.168.0.10' => '',
                'staging2.glomp.it' => '', //need this
            );

            $url_array = explode("/", base_url());
            $from_hactivate_live = isset($url_array[3]) ? $url_array[3] : '';
            // // Top-Level Keys http://developer.apple.com/library/ios/#documentation/userexperience/Reference/PassKit_Bundle/Chapters/TopLevel.html

            if ( isset($domain_names[$_SERVER['HTTP_HOST']]) == true) { //if domain listed
                //for testing
                $webServiceURL = 'https://staging2.glomp.it/hactivate/IosVoucher/webservice/'.$campaign_id.'/';
                $redeem_url = base_url('landing/page/carlsberg/redeem/confirm/ios/' . $client);

                //from live
               if ($from_hactivate_live == 'hactivate_live' || $_SERVER['HTTP_HOST'] == 'beerwithfood.me' || $_SERVER['HTTP_HOST'] == 'www.beerwithfood.me' ) { //if from staging2.glomp.it/hactivate_live or beerwithfood.me
                    $webServiceURL = 'https://staging2.glomp.it/hactivate_live/IosVoucher/webservice/'.$campaign_id.'/'; //support SSL workaround
                    $redeem_url = 'http://beerwithfood.me/landing/page/carlsberg/redeem/confirm/ios/' . $client; //since webservice will download the new voucher
                }

            } else {
                //from local?
                $webServiceURL = base_url('IosVoucher/webservice/'.$campaign_id.'/');
                $redeem_url = base_url('landing/page/carlsberg/redeem/confirm/ios/' . $client);
            }

            $standardKeys         = [ 
                'webServiceURL'      => $webServiceURL,
//                'webServiceURL'      => 'https://192.168.1.107/hactivate/IosVoucher/webservice/'.$campaign_id.'/',
                'passTypeIdentifier' => 'pass.com.hactvat8',
                'teamIdentifier'     => 'UD4F7T7J39',
                'description'        => "Carlsberg",
                'formatVersion'      => 1,
                'organizationName'   => "Carlsberg", 
                'serialNumber'       => $serial,
                "authenticationToken" => "PZ8GL23T24JEB41X6U"
                           // 4. Set to yours
            ];

            $associatedAppKeys    = [];

            $relevanceKeys        = [
                'description' => 'Carlsberg',
                'maxDistance' =>1000,
                'locations' => [ 
                    [
                        'latitude'   => 22.296326,
                        'longitude' => 114.01668910000001,
                        'relevantText' => 'Coyote Discovery Bay - Shop G08A, G/F, D\'Deck, Discovery Bay',
                    ], 
                    [ 
                        'latitude'   => 22.2779393,
                        'longitude' =>  114.17252289999999,
                        'relevantText' => 'Coyote Wanchai - G/F, Gaylord Commercial Building,114-120 Lockhart Road, Wanchai',
                    ],
                    [ 
                        'latitude'   => 22.282621,
                        'longitude' => 114.15198599999997,
                        'relevantText' => 'McSorley\'s Soho - G/F, 46 Staunton Street, Soho, Central, HK',
                    ],
                    [ 
                        'latitude'   => 22.2963115,
                        'longitude' => 114.0161435,
                        'relevantText' => 'McSorley\'s Discovery Bay - Block B, G/F, D¹Deck, Discovery Bay, HK',
                    ],
                    [ 
                        'latitude'   => 22.3043347,
                        'longitude' => 114.16103550000003,
                        'relevantText' => 'Grand Central - Shop R001, Roof Level, Civic Square, Elements, 1 Austin Road West, Tsim Sha Tsui',
                    ],
                    [ 
                        'latitude'   => 22.2702247,
                        'longitude' => 114.18457880000005,
                        'relevantText' => 'The Jockey - 33 Wong Nai Chung Road, Happy Valley',
                    ],
                    [ 
                        'latitude'   => 22.3012194,
                        'longitude' => 114.17329110000003,
                        'relevantText' => 'Wildfire Knutsford Terrace - G/F, 2 Knutsford Terrace, Tsim Sha Tsui',
                    ],
                    // [ 
                    //     'latitude'   => 22.2863336,
                    //     'longitude' => 114.22354089999999,
                    //     'relevantText' => 'Wildfire Soho East - Shop 2B-07, G/F, 45 Tai Hong Street, Site B, Soho East, Lei King Wan, Sai Wan Ho',
                    // ],
                    [ 
                        'latitude'   => 22.2712334,
                        'longitude' => 114.15026439999997,
                        'relevantText' => 'Wildfire The Peak - Shop 2, 1/F, The Peak Tower, 128 Peak Road, The Peak',
                    ],
                    [ 
                        'latitude'   => 22.282298,
                        'longitude' => 114.18458899999996,
                        'relevantText' => 'Wildfire Fashion Walk - Shop C-D, G/F, 59-65 Paterson Street, Causeway Ba',
                    ],
                ],
            ];

            // $styleKeys            = $voucher_data['style_keys']; 
           
            $styleKeys  = [
                'eventTicket' => [
                    'headerFields' => [
                        [
                            'key' => '',
                            'value' => 'Carlsberg HK',
                            'label' => 'Store',
                            'changeMessage' => 'The store name has been updated.',
                            'textAlignment' => 'PKTextAlignmentRight',
                        ]
                    ],
                    'secondaryFields' => [
                        [
                            'key' => 'item',
                            'label' => 'Valid Date: 9 – 23 Feb 2018',
                            'value' => '有效日期: 2018年2月9-23日',
                        ],
                        // [
                        //     'key' => 'special-price',
                        //     'label' => 'SPECIAL PRICE',
                        //     'value' => '$10 (U.P. $20)',
                        //     'textAlignment' => 'PKTextAlignmentRight',
                        // ]
                    ],
                    'backFields' => [
                        [
                            'key'   => 'description',
                            'label' => '',
                            'value' => 'Redemption Locations:

1. Coyote Discovery Bay: Shop G08A, G/F, D\'Deck, Discovery Bay, Lantau Island / Tel: 2987 2848

2. Coyote Wanchai: G/F, Gaylord Commercial Building, 114-120 Lockhart Road, Wan Chai, HK / Tel: 2861 2221

3. Grand Central: Shop R001, Roof Level, Civic Square, Elements, 1 Austin Road West, Tsim Sha Tsui, KLN / Tel: 2736 4888

4. McSorley’s Discovery Bay: Block B, G/F, D’Deck, Discovery Bay, Lantau Island / Tel: 2987 8280

5. McSorley’s Soho: G/F, 46 Staunton Street, Soho, Central, HK / Tel: 2522 2646

6. The Jockey: 33 Wong Nai Chung Road, Happy Valley, HK / Tel: 2572 2266

7. Wildfire Fashion Walk: Shop C-D, G/F, 59-65 Paterson Street, Causeway Bay, HK / Tel: 2352 0198

8. Wildfire Knutsford Terrace: G/F, 2 Knutsford Terrace, Tsim Sha Tsui, KLN / Tel: 3690 1598

9. Wildfire The Peak: Shop 2, 1/F, The Peak Tower, 128 Peak Road, The Peak, HK / Tel: 2849 5123


Terms & Conditions:

1. Promotion period: 9 – 23 February, 2018
2. This coupon can be used in above outlets.
3. This coupon can redeem Carlsberg draught beer buy 1 get 1 free offer and selected food at 50% off once only.
4. Please present this coupon before ordering. Photocopies and screen shot will not be accepted.
5. This coupon is not transferable, cash redeemable or refundable.
6. Consumer who wants to opt out from receiving notification, please delete the coupon.
7. This coupon can be used in conjunction with the Happy Hour offers but not with any other credit card or staff offers.
8. Carlsberg Hong Kong Limited reserves the right to make the final decision in the event of any dispute.
9. Enquiry hotline: 3189 8222 / 3189 8224.

使用之條款及細則：

1. 優惠期: 2018年2月9至23日
2. 此券適用於上述指定商店。
3. 此券只可使用一次。此券可兌換嘉士伯生啤買一送一優惠以及額外以半價享用指定食物一客。
4. 請於點菜前出示此券；影印本或屏幕載圖恕不接受。
5. 此券不可兌換現金或轉讓，亦不設找贖。
6. 如欲停止接收通知提示，請刪除優惠卷。
7. 此券可與Happy Hour優惠同時使用，但不可與其他信用卡或員工優惠同時使用。
8. 如有任何爭議，一概以嘉士伯香港有限公司的最終決定為準。
9. 如有任何查詢，請致電 3189 8222 / 3189 8224。


* Please press Redeem button in front of bar / restaurant staffs.
* 請於到達餐廳/酒吧後，於店員面前按下Redeem。',
                        ],
                        [
                            'key'   => 'redeem',
                            'label' => 'Info',
                            'value' => '<a href="' . $redeem_url . '">Redeem</a>' ,
                        ],
                    ],
                ],
            ];

            $voucher = $this->db->get_where('h_campaign_ios_vouchers', array(
                'customer_id' => $client_id,
                'campaign_id' => $campaign_id
            ));

            if ($voucher->num_rows() > 0) {
                $property = json_decode($voucher->row()->properties);
                // echo '<pre>';
                // print_r($property); 

                //change please
                if (isset($property->backFields) && count($property->backFields) >0 ) {
                    $styleKeys['generic']['backFields'][] = (array) $property->backFields[0]; //add new message   
                }
            }

            //$visualAppearanceKeys = $voucher_data['visual_appearance_keys'];
            
            $visualAppearanceKeys = [
                'foregroundColor' => 'rgb(250, 250, 250)',
                'backgroundColor' => 'rgb(7,30,27)',
                'labelColor' => 'rgb(250, 250, 250)',
                'logoText' => '',
            ];            
     
        
        } //end carlsberg

        if ($campaign_id == 6 ) {
        // uemura

            $domain_names = array(
                'event.hkshugirls.com' => '',
                'www.event.hkshugirls.com' => '', 
                '192.168.0.12' => '',
                'staging2.glomp.it' => '', //need this
            );

            $url_array = explode("/", base_url());
            $from_hactivate_live = isset($url_array[3]) ? $url_array[3] : '';
            // // Top-Level Keys http://developer.apple.com/library/ios/#documentation/userexperience/Reference/PassKit_Bundle/Chapters/TopLevel.html

            if ( isset($domain_names[$_SERVER['HTTP_HOST']]) == true) { //if domain listed
                //for testing
                $webServiceURL = 'https://staging2.glomp.it/hactivate/IosVoucher/webservice/'.$campaign_id.'/';
                $redeem_url = base_url('landing/page/uemura/redeem/confirm/ios/' . $client);

                //from live
               if ($from_hactivate_live == 'hactivate_live' || $_SERVER['HTTP_HOST'] == 'event.hkshugirls.com' || $_SERVER['HTTP_HOST'] == 'www.event.hkshugirls.com' ) { //if from staging2.glomp.it/hactivate_live or event.hkshugirls.com
                    $webServiceURL = 'https://staging2.glomp.it/hactivate_live/IosVoucher/webservice/'.$campaign_id.'/'; //support SSL workaround
                    $redeem_url = 'http://event.hkshugirls.com/landing/page/uemura/redeem/confirm/ios/' . $client; //since webservice will download the new voucher
                }

            } else {
                //from local?
                $webServiceURL = base_url('IosVoucher/webservice/'.$campaign_id.'/');
                $redeem_url = base_url('landing/page/uemura/redeem/confirm/ios/' . $client);
            }

            $standardKeys         = [ 
                'webServiceURL'      => $webServiceURL,
//                'webServiceURL'      => 'https://192.168.1.107/hactivate/IosVoucher/webservice/'.$campaign_id.'/',
                'passTypeIdentifier' => 'pass.com.hactvat8',
                'teamIdentifier'     => 'UD4F7T7J39',
                'description'        => "Shu Uemura",
                'formatVersion'      => 1,
                'organizationName'   => "Shu Uemura", 
                'serialNumber'       => $serial,
                "authenticationToken" => "PZ8GL23T24JEB41X6U"
                           // 4. Set to yours
            ];

            $associatedAppKeys    = [];

            $relevanceKeys        = [
                'description' => 'Shu Uemura',
                'maxDistance' =>1000,
                'locations' => [
                    [
                        'latitude'   => 14.51429,
                        'longitude' => 121.04525,
                        'relevantText' => '立即領取專屬你的植村秀 #mattitude gift!',
                    ],  
                    [
                        'latitude'   => 22.295260178185153,
                        'longitude' => 114.16828340463974,
                        'relevantText' => '立即領取專屬你的植村秀 #mattitude gift!',
                    ],
                ],
            ];

            // $styleKeys            = $voucher_data['style_keys']; 
           
            $styleKeys  = [
                'eventTicket' => [
                    'headerFields' => [
                        [
                            'key' => '',
                            'value' => 'Shu Uemura',
                            'label' => '',
                            'changeMessage' => 'The store name has been updated.',
                            'textAlignment' => 'PKTextAlignmentRight',
                        ]
                    ],
                    'secondaryFields' => [
                        [
                            'key' => 'item',
                            'label' => '立即前往植村秀位於海港城海運大廈入口大堂的 #mattitude pop-up store',
                            'value' => '領取專屬你的 #mattitude gift!',
                        ],
                        // [
                        //     'key' => 'special-price',
                        //     'label' => 'SPECIAL PRICE',
                        //     'value' => '$10 (U.P. $20)',
                        //     'textAlignment' => 'PKTextAlignmentRight',
                        // ]
                    ],
                    'backFields' => [
                        [
                            'key'   => 'description',
                            'label' => '',
                            'value' => '',
                        ],
                        [
                            'key'   => 'redeem',
                            'label' => 'Info',
                            'value' => '<a href="' . $redeem_url . '">Redeem</a>' ,
                        ],
                    ],
                ],
            ];

            $voucher = $this->db->get_where('h_campaign_ios_vouchers', array(
                'customer_id' => $client_id,
                'campaign_id' => $campaign_id
            ));

            if ($voucher->num_rows() > 0) {
                $property = json_decode($voucher->row()->properties);
                // echo '<pre>';
                // print_r($property); 

                //change please
                if (isset($property->backFields) && count($property->backFields) >0 ) {
                    $styleKeys['generic']['backFields'][] = (array) $property->backFields[0]; //add new message   
                }
            }

            //$visualAppearanceKeys = $voucher_data['visual_appearance_keys'];
            
            $visualAppearanceKeys = [
                'foregroundColor' => 'rgb(255, 255, 255)',
                'backgroundColor' => 'rgb(0, 0, 0)',
                'labelColor' => 'rgb(255, 255, 255)',
                'logoText' => '',
            ];            
     
        
        } //end uemera


        if ($campaign_id == 7 ) {
        // cartier

            $domain_names = array(
                'princecampaign.dtwdigital.com.hk' => '',
                'www.princecampaign.dtwdigital.com.hk' => '', 
                '192.168.0.11' => '',
                'staging2.glomp.it' => '', //need this
            );

            $url_array = explode("/", base_url());
            $from_hactivate_live = isset($url_array[3]) ? $url_array[3] : '';
            // // Top-Level Keys http://developer.apple.com/library/ios/#documentation/userexperience/Reference/PassKit_Bundle/Chapters/TopLevel.html

            if ( isset($domain_names[$_SERVER['HTTP_HOST']]) == true) { //if domain listed
                //for testing
                $webServiceURL = 'https://staging2.glomp.it/hactivate/IosVoucher/webservice/'.$campaign_id.'/';
                $redeem_url = base_url('landing/page/cartier/redeem/confirm/ios/' . $client);

                //from live
               if ($from_hactivate_live == 'hactivate_live' || $_SERVER['HTTP_HOST'] == 'princecampaign.dtwdigital.com.hk' || $_SERVER['HTTP_HOST'] == 'www.princecampaign.dtwdigital.com.hk' ) { //if from staging2.glomp.it/hactivate_live or princecampaign.dtwdigital.com.hk
                    $webServiceURL = 'https://staging2.glomp.it/hactivate_live/IosVoucher/webservice/'.$campaign_id.'/'; //support SSL workaround
                    $redeem_url = 'http://princecampaign.dtwdigital.com.hk/landing/page/cartier/redeem/confirm/ios/' . $client; //since webservice will download the new voucher
                }

            } else {
                //from local?
                $webServiceURL = base_url('IosVoucher/webservice/'.$campaign_id.'/');
                $redeem_url = base_url('landing/page/cartier/redeem/confirm/ios/' . $client);
            }

            $standardKeys         = [ 
                'webServiceURL'      => $webServiceURL,
//                'webServiceURL'      => 'https://192.168.1.107/hactivate/IosVoucher/webservice/'.$campaign_id.'/',
                'passTypeIdentifier' => 'pass.com.hactvat8',
                'teamIdentifier'     => 'UD4F7T7J39',
                'description'        => "太子珠寶鐘錶",
                'formatVersion'      => 1,
                'organizationName'   => "太子珠寶鐘錶", 
                'serialNumber'       => $serial,
                "authenticationToken" => "PZ8GL23T24JEB41X6U"
                           // 4. Set to yours
            ];

            $associatedAppKeys    = [];

            $relevanceKeys        = [
                'description' => '太子珠寶鐘錶呈獻專屬禮遇',
                'maxDistance' =>1000,
                'locations' => [
                    [
                        'latitude'   => 14.51429,
                        'longitude' => 121.04525,
                        'relevantText' => 'BAHAY',
                    ],  
                    [
                        'latitude'   => 22.296013,
                        'longitude' => 114.17192,
                        'relevantText' => '親臨太子珠寶鐘錶專門店領取卡地亞專屬禮遇。數量有限，送完即止。',
                    ],
                    [
                        'latitude'   => 22.2965161,
                        'longitude' => 114.17073720000008,
                        'relevantText' => '親臨太子珠寶鐘錶專門店領取卡地亞專屬禮遇。數量有限，送完即止。',
                    ],
                    [
                        'latitude'   => 22.2957533,
                        'longitude' => 114.16910699999994,
                        'relevantText' => '親臨太子珠寶鐘錶專門店領取卡地亞專屬禮遇。數量有限，送完即止。',
                    ],
                    [
                        'latitude'   => 22.2790864,
                        'longitude' => 114.18293470000003,
                        'relevantText' => '親臨太子珠寶鐘錶專門店領取卡地亞專屬禮遇。數量有限，送完即止。',
                    ],
                    [
                        'latitude'   => 14.5567,
                        'longitude' => 121.015,
                        'relevantText' => 'Yay! You are at O2Space.',
                    ],
                ],
            ];

            // $styleKeys            = $voucher_data['style_keys']; 
           
            $styleKeys  = [
                'eventTicket' => [
                    'headerFields' => [
                        [
                            'key' => '',
                            'value' => '太子珠寶鐘錶',
                            'label' => '立即親臨',
                            'changeMessage' => 'The store name has been updated.',
                            'textAlignment' => 'PKTextAlignmentRight',
                        ]
                    ],
                    'secondaryFields' => [
                        [
                            'key' => 'item',
                            'label' => '10月12日或之前選購卡地亞腕錶並領取禮遇（數量有限，送完即止）',
                            'value' => '請按「i」圖示以領取禮遇並了解詳情。',
                        ],
                        // [
                        //     'key' => 'special-price',
                        //     'label' => 'SPECIAL PRICE',
                        //     'value' => '$10 (U.P. $20)',
                        //     'textAlignment' => 'PKTextAlignmentRight',
                        // ]
                    ],
                    'backFields' => [
                        [
                            'key'   => 'description',
                            'label' => '',
                            'value' => '誠邀您蒞臨以下太子珠寶鐘錶專門店，探索卡地亞作品系列。與此同時，於2018年10月12日或之前選購卡地亞腕錶並出示此禮遇專函，即可獲享卡地亞專屬禮遇乙份（數量有限，送完即止）。 
彌敦道總店
九龍尖沙咀彌敦道23-25號彩星中心地下及地庫

太子集團中心分店
九龍尖沙咀北京道12號A地下及1樓

海洋中心卡地亞專門店
九龍尖沙咀海港城海洋中心3樓319號舖

羅素街分店
銅鑼灣羅素街58號鋪地下至3樓B-C號鋪

請於太子珠寶鐘錶專門店領取禮遇時才點擊下方「領取禮遇」連結。',
                        ],
                        [
                            'key'   => 'redeem',
                            'label' => 'Info',
                            'value' => '<a href="' . $redeem_url . '">領取禮遇</a>' ,
                        ],
                    ],
                ],
            ];

            $voucher = $this->db->get_where('h_campaign_ios_vouchers', array(
                'customer_id' => $client_id,
                'campaign_id' => $campaign_id
            ));

            if ($voucher->num_rows() > 0) {
                $property = json_decode($voucher->row()->properties);
                // echo '<pre>';
                // print_r($property); 

                //change please
                if (isset($property->backFields) && count($property->backFields) >0 ) {
                    $styleKeys['generic']['backFields'][] = (array) $property->backFields[0]; //add new message   
                }
            }

            //$visualAppearanceKeys = $voucher_data['visual_appearance_keys'];
            
            $visualAppearanceKeys = [
                'foregroundColor' => 'rgb(255, 255, 255)',
                'backgroundColor' => 'rgb(189, 9, 20)',
                'labelColor' => 'rgb(255, 255, 255)',
                'logoText' => '',
            ];            
     
        
        } //end cartier
        
        $webServiceKeys       = [];
        
        
        // Merge all pass data and set JSON for $pass object
        $passData = array_merge(
            $standardKeys,
            $associatedAppKeys,
            $relevanceKeys,
            $styleKeys,
            $visualAppearanceKeys,
            $webServiceKeys
        );

        $pass->setJSON(json_encode($passData));

        // Add files to the PKPass package
        if ($campaign_id == 1) {
            // ETO DIN PANG MARTELL
            $pass->addFile('assets/images/voucher/icon.png');
            $pass->addFile('assets/images/voucher/icon@2x.png');
            $pass->addFile('assets/images/voucher/logo.png');
            $pass->addFile('assets/images/voucher/thumbnail.png');
        }

        if ($campaign_id == 2) {
            $pass->addFile('assets/images/issey-miyake/voucher/icon.png');
            $pass->addFile('assets/images/issey-miyake/voucher/icon@2x.png');
            $pass->addFile('assets/images/issey-miyake/voucher/logo.png');
            // $pass->addFile('assets/images/issey-miyake/voucher/thumbnail.png');
        }
        
        if ($campaign_id == 9999) {
            $pass->addFile('assets/images/issey-miyake/voucher/icon.png');
            $pass->addFile('assets/images/issey-miyake/voucher/icon@2x.png');
            $pass->addFile('assets/images/issey-miyake/voucher/logo.png'); 
        }

        if ($campaign_id == 3) {
            $pass->addFile('assets/images/bodyshop/icon.png');
            $pass->addFile('assets/images/bodyshop/icon@2x.png');
            $pass->addFile('assets/images/bodyshop/logo.png');
            $pass->addFile('assets/images/bodyshop/logo@2x.png');
            $pass->addFile('assets/images/bodyshop/strip.png');
            $pass->addFile('assets/images/bodyshop/strip@2x.png');
        }

        if ($campaign_id == 4) {
            $pass->addFile('assets/images/bodyshop-b/icon.png');
            $pass->addFile('assets/images/bodyshop-b/icon@2x.png');
            $pass->addFile('assets/images/bodyshop-b/logo.png');
            $pass->addFile('assets/images/bodyshop-b/logo@2x.png');
            $pass->addFile('assets/images/bodyshop-b/strip.png');
            $pass->addFile('assets/images/bodyshop-b/strip@2x.png');
        }

        if ($campaign_id == 5) {
            $pass->addFile('assets/images/carlsberg/icon.png');
            $pass->addFile('assets/images/carlsberg/icon@2x.png');
            $pass->addFile('assets/images/carlsberg/strip.png');
            $pass->addFile('assets/images/carlsberg/strip@2x.png');
        }

        if ($campaign_id == 6) {
            $pass->addFile('assets/images/uemura/icon.png');
            $pass->addFile('assets/images/uemura/icon@2x.png');
            $pass->addFile('assets/images/uemura/strip.png');
            $pass->addFile('assets/images/uemura/strip@2x.png');
        }

        if ($campaign_id == 7) {
            $pass->addFile('assets/images/cartier/icon.png');
            $pass->addFile('assets/images/cartier/icon@2x.png');
            $pass->addFile('assets/images/cartier/strip.png');
            $pass->addFile('assets/images/cartier/strip@2x.png');
        }
        
        if ( !$pass->create(true)) { // Create and output the PKPass
            echo 'Error: ' . $pass->getError();
        }

    }

    public function createVoucherHk($campaign_id, $client_id, $serial_number = "")
    {
        $pass = new PKPass();

        $pass->setCertificate('assets/certificates/hactivatePassId.p12');  // 1. Set the path to your Pass Certificate (.p12 file)
        //$pass->setCertificatePassword('P@$$w0rd123');     // 2. Set password for certificate
        //$pass->setCertificatePassword('');     // 2. Set password for certificate
        $pass->setWWDRcertPath('assets/certificates/AWWDR.pem'); // 3. Set the path to your WWDR Intermediate certificate (.pem file)
        $serial = "";
        $client = "";
        if (strlen($serial_number) > 0) {
            $serial_array = explode("-",$serial_number);
            $serial = $serial_number;
            $client = $serial_array[1];
        } else {
            $serial = $campaign_id . "-" . $client_id;
            $client = $client_id;
        }

        // // Pass content
        // $data = [
        //     'description' => 'Demo pass',
        //     'formatVersion' => 1,
        //     'organizationName' => 'Flight Express',
        //     'passTypeIdentifier' => 'pass.com.hactvat8', // Change this!
        //     'serialNumber' => '12345678',
        //     'teamIdentifier' => 'UD4F7T7J39', // Change this!
        //     'boardingPass' => [
        //         'primaryFields' => [
        //             [
        //                 'key' => 'origin',
        //                 'label' => 'San Francisco',
        //                 'value' => 'SFO',
        //             ],
        //             [
        //                 'key' => 'destination',
        //                 'label' => 'London',
        //                 'value' => 'LHR',
        //             ],
        //         ],
        //         'secondaryFields' => [
        //             [
        //                 'key' => 'gate',
        //                 'label' => 'Gate',
        //                 'value' => 'F12',
        //             ],
        //             [
        //                 'key' => 'date',
        //                 'label' => 'Departure date',
        //                 'value' => '07/11/2012 10:22',
        //             ],
        //         ],
        //         'backFields' => [
        //             [
        //                 'key' => 'passenger-name',
        //                 'label' => 'Passenger',
        //                 'value' => 'John Appleseed',
        //             ],
        //         ],
        //         'transitType' => 'PKTransitTypeAir',
        //     ],
        //     'barcode' => [
        //         'format' => 'PKBarcodeFormatQR',
        //         'message' => 'Flight-GateF12-ID6643679AH7B',
        //         'messageEncoding' => 'iso-8859-1',
        //     ],
        //     'backgroundColor' => 'rgb(32,110,247)',
        //     'logoText' => 'Flight info',
        //     'relevantDate' => date('Y-m-d\TH:i:sP')
        // ];
        // $pass->setJSON(json_encode($data));

         if ($campaign_id == 2) {
            // // Top-Level Keys http://developer.apple.com/library/ios/#documentation/userexperience/Reference/PassKit_Bundle/Chapters/TopLevel.html
            $standardKeys         = [ 
                'webServiceURL'      => 'https://119.9.88.193/IosVoucher/webservice/',
                'passTypeIdentifier' => 'pass.com.hactvat8',
                'teamIdentifier'     => 'UD4F7T7J39',
                'description'        => "Issey Miyake",
                'formatVersion'      => 1,
                'organizationName'   => "Issey Miyake", 
                'serialNumber'       => $serial,
                "authenticationToken" => "PZ8GL23T24JEB41X6U"
                           // 4. Set to yours
            ];
             
            
            $associatedAppKeys    = [];
            $relevanceKeys        = [
                'maxDistance' => 200,
                'locations' => [
                    [ 
                        'latitude'   => 22.317934,
                        'longitude' => 114.16869399999996,
                        'relevantText' => 'Store nearby on Beauty Avenue (BA108b, Level 1)',
                    ],
                    [ 
                        'latitude'   => 22.29507,
                        'longitude' => 114.16712299999995,
                        'relevantText' => 'Store nearby on Shop 202',
                    ],
                    [ 
                        'latitude'   => 22.2953498,
                        'longitude' => 114.16900710000004,
                        'relevantText' => 'Store nearby on Shop 201, 2/F',
                    ], 
                    [ 
                        'latitude'   => 22.2953498,
                        'longitude' => 114.16900710000004,
                        'relevantText' => 'Store nearby on Pop-up Store ­ G/F (next to escalator towards LG Level)',
                    ], 
                    [ 
                        'latitude'   => 22.2802473,
                        'longitude' => 114.18430940000007,
                        'relevantText' => 'Store nearby on SOGO Causeway Bay ­B1',
                    ],
                    [ 
                        'latitude'   => 22.1910054,
                        'longitude' => 113.540392,
                        'relevantText' => 'Store nearby on New Yaohan Macau ­1/F',
                    ],             
                    [ 
                        'latitude'   => 22.2811499,
                        'longitude' => 114.15260940000007,
                        'relevantText' => 'No internet test',
                    ],   
                    [
                        'latitude'   => 14.5614206,
                        'longitude' => 121.01424700000007,
                        'relevantText' => 'Hey allan testing',
                    ]
                    
                ],
            ];

            // $styleKeys            = $voucher_data['style_keys']; 
           
            $styleKeys  = [
                'generic' => [
                    'primaryFields'   => [
                        [
                            'key'   => 'origin',
                            'label' => '體驗全新',
                            'value' => 'L\'Eau d\'Issey Pure淡香氛',
                        ] 
                    ],
                    'secondaryFields' => [
                        [
                            'key'   => 'value1',
                            'label' => '為您帶來的寧靜時刻。',
                            'value' => '出示此電子換領券，並到您所選擇之香氛專櫃，免費領取試用裝一份。數量有限，換完即止。',
                        ], 
                        /**
                        [
                            'key'   => 'date',
                            'label' => 'Secondary Field 2',
                            'value' => 'http://google.com',
                        ],
                        **/

                    ],
                    'auxiliaryFields' => [
                        [
                            'key'   => 'value1',
                            'label' => '',
                            'value' => '請按下 "i" 尋找店舖位置以領取試用裝',
                        ],
                        
                    ],
                    'backFields'      => [
                        [
                            'key'   => 'passenger-name',
                            'label' => 'Info',
                            'value' => '<a style="font-size:25px" href="' . base_url('landing/page/issey-miyake-hk/redeem/confirm/ios/' . $client) . '">Redeem</a>' ,
                        ],
                        [
                            'key'   => 'addresses',
                            'label' => 'Redemption Outlet Addresses',
                            'value' => "連卡佛 (廣東道)\n尖沙咀廣東道3號海運大廈二樓連卡佛\r\n\r\n尖沙咀海運大廈FACESSS\n尖沙咀廣東道3號海運大廈二樓FACESSS\r\n\r\n崇光銅鑼灣店\n銅鑼灣崇光百貨B1\r\n\r\nBeauty Avenue朗豪坊\n九龍旺角朗豪坊一樓Beauty Avenue\r\n\r\n澳門新八佰伴\n澳門蘇亞利斯博士大馬路一樓1-17\r\n\r\napm Pop-up Store\n觀塘觀塘道418號apm地下 (近通往LG之扶手電梯)" ,
                        ],
                    ]
                ],
            ];

            //$visualAppearanceKeys = $voucher_data['visual_appearance_keys'];
            
            $visualAppearanceKeys = [
                'foregroundColor' => 'rgb(0,0,0)',
                'backgroundColor' => 'rgb(241, 245, 243)',
                'labelColor' => 'rgb(0,0,0)',
                'logoText'        => 'Issey Miyake',
            ];
        }
        
        $webServiceKeys       = [];
        
        
        // Merge all pass data and set JSON for $pass object
        $passData = array_merge(
            $standardKeys,
            $associatedAppKeys,
            $relevanceKeys,
            $styleKeys,
            $visualAppearanceKeys,
            $webServiceKeys
        );

        $pass->setJSON(json_encode($passData));

        if ($campaign_id == 2) {
            $pass->addFile('assets/images/issey-miyake/voucher/icon.png');
            $pass->addFile('assets/images/issey-miyake/voucher/icon@2x.png');
            $pass->addFile('assets/images/issey-miyake/voucher/logo.png');
            // $pass->addFile('assets/images/issey-miyake/voucher/thumbnail.png');
        }
        
        
        if ( !$pass->create(true)) { // Create and output the PKPass
            echo 'Error: ' . $pass->getError();
        }

    }
}