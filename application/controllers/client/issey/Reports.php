<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends CI_Controller {

	var $hk_outlets = '3,4,5,6,7,8';

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function page_visitor() 
	{
		$data['sidebar_view'] = 'client/issey/sidebar';

		if (isset($_POST['draw'])) {
			echo json_encode($this->_page_visitors());
			return;
		}

		$this->load->view('client/issey/reports/page_visitors', $data);
	}

	public function page_visitor_hk() 
	{
		$data['sidebar_view'] = 'client/issey/sidebar';

		if (isset($_POST['draw'])) {
			echo json_encode($this->_page_visitors_hk());
			return;
		}

		$this->load->view('client/issey/reports/page_visitors_hk', $data);
	}

	public function registrants() 
	{
		$data['sidebar_view'] = 'client/issey/sidebar';

		if (isset($_POST['draw'])) {
			echo json_encode($this->_registrants());
			return;
		}

		$this->load->view('client/issey/reports/registrants', $data);
	}

	public function registrants_hk() 
	{
		$data['sidebar_view'] = 'client/issey/sidebar';

		if (isset($_POST['draw'])) {
			echo json_encode($this->_registrants_hk());
			return;
		}

		$this->load->view('client/issey/reports/registrants_hk', $data);
	}

	public function download_from_android() 
	{
		$data['sidebar_view'] = 'client/issey/sidebar';

		if (isset($_POST['draw'])) {
			echo json_encode($this->_download_from_android());
			return;
		}

		$this->load->view('client/issey/reports/download_from_android', $data);
	}

	public function download_from_android_hk() 
	{
		$data['sidebar_view'] = 'client/issey/sidebar';

		if (isset($_POST['draw'])) {
			echo json_encode($this->_download_from_android_hk());
			return;
		}

		$this->load->view('client/issey/reports/download_from_android_hk', $data);
	}

	public function download_from_ios() 
	{

		$data['sidebar_view'] = 'client/issey/sidebar';

		if (isset($_POST['draw'])) {
			echo json_encode($this->_download_from_ios());
			return;
		}

		$this->load->view('client/issey/reports/download_from_ios', $data);
	}

	public function download_from_ios_hk() 
	{

		$data['sidebar_view'] = 'client/issey/sidebar';

		if (isset($_POST['draw'])) {
			echo json_encode($this->_download_from_ios_hk());
			return;
		}

		$this->load->view('client/issey/reports/download_from_ios_hk', $data);
	}

	public function redemptions($channel = 'ios') 
	{
		$data['sidebar_view'] = 'client/issey/sidebar';

		if (isset($_POST['draw'])) {
			echo json_encode($this->_redemptions($channel));
			return;
		}


		$this->load->view('client/issey/reports/redemptions', $data);
	}

	public function redemptions_hk($channel = 'ios') 
	{
		$data['sidebar_view'] = 'client/issey/sidebar';

		if (isset($_POST['draw'])) {
			echo json_encode($this->_redemptions_hk($channel));
			return;
		}


		$this->load->view('client/issey/reports/redemptions_hk', $data);
	}

	public function graphs() {
		$campaign_id = 2;
		$month = 7;
		$year = 2017;

		$start_date = date("Y-m-d", strtotime($year.'-'.$month.'-01'));
		$end_date = date("Y-m-t", strtotime($year.'-'.$month.'-01'));

		//no. of page visitor
		$this->db->select('DAY(date_created) AS DAY, COUNT(*) AS total');
		$this->db->like('logs', '"action":"visitor","page":"landing"');
		$this->db->group_by('DAY(date_created)'); 
		$this->db->where('date_created >= "' . $start_date . ' 00:00:00" AND date_created <="' . $end_date . ' 11:59:59"' ); 
		$res1 = $this->db->get_where('h_campaign_log', array('campaign_id' => $campaign_id));

		//no. of registrants
		$this->db->select('DAY(date_created) AS DAY, COUNT(*) AS total');
		$this->db->like('logs', '"action":"register","page":"register-popup"');
		$this->db->group_by('DAY(date_created)'); 
		$this->db->where('date_created >= "' . $start_date . ' 00:00:00" AND date_created <="' . $end_date . ' 11:59:59"' ); 
		$res2 = $this->db->get_where('h_campaign_log', array('campaign_id' => $campaign_id));

		//no. of ios downloads
		$this->db->select('DAY(date_created) AS DAY, COUNT(*) AS total');
		$this->db->like('logs', '"action":"download","page":"landing\/ios"');
		$this->db->group_by('DAY(date_created)'); 
		$this->db->where('date_created >= "' . $start_date . ' 00:00:00" AND date_created <="' . $end_date . ' 11:59:59"' ); 
		$res3 = $this->db->get_where('h_campaign_log', array('campaign_id' => $campaign_id));

		//no. of android downloads
		$this->db->select('DAY(date_created) AS DAY, COUNT(*) AS total');
		$this->db->like('logs', '"action":"download","page":"landing\/android"');
		$this->db->group_by('DAY(date_created)'); 
		$this->db->where('date_created >= "' . $start_date . ' 00:00:00" AND date_created <="' . $end_date . ' 11:59:59"' ); 
		$res4 = $this->db->get_where('h_campaign_log', array('campaign_id' => $campaign_id));


		//no. of redempions ios
		$this->db->select('DAY(date_created) AS DAY, COUNT(*) AS total');
		$this->db->like('logs', '"action":"download","page":"landing\/android"');
		$this->db->group_by('DAY(date_created)'); 
		$this->db->where('date_created >= "' . $start_date . ' 00:00:00" AND date_created <="' . $end_date . ' 11:59:59"' ); 
		$res5 = $this->db->get_where('h_campaign_log', array('campaign_id' => $campaign_id));

		$graphs = array(
			'visitors' => array(),
			'registrants' => array(),
			'ios' => array(),
			'android' => array(),
			'labels' => array()
		);

		$visitor_result = array();
		$registrant_result = array();
		$ios_downloads = array();
		$android_downloads = array();

		//need to create an array to save result
		foreach ($res1->result_array() as $v) {
			$visitor_result[$v['DAY']] = $v['total'];
		}

		//need to create an array to save result
		foreach ($res2->result_array() as $v) {
			$registrant_result[$v['DAY']] = $v['total'];
		}

		//need to create an array to save result
		foreach ($res3->result_array() as $v) {
			$ios_downloads[$v['DAY']] = $v['total'];
		}

		//need to create an array to save result
		foreach ($res4->result_array() as $v) {
			$android_downloads[$v['DAY']] = $v['total'];
		}


		for($d = 1; $d <= 31; $d++)
		{
		    $time=mktime(12, 0, 0, $month, $d, $year);          
		    if (date('m', $time) == $month) {
		    	$graphs['labels'][] = date('M d, Y', $time);

		    	//vistors
		    	if (isset($visitor_result[$d])) {
		    		//get total and save into array
		    		$graphs['visitors'][] = $visitor_result[$d];
		    	} else {
		    		$graphs['visitors'][] = 0;
		    	}

		    	//registrants
		    	if (isset($registrant_result[$d])) {
		    		//get total and save into array
		    		$graphs['registrants'][] = $registrant_result[$d];
		    	} else {
		    		$graphs['registrants'][] = 0;
		    	}

		    	//ios downloads
		    	if (isset($ios_downloads[$d])) {
		    		//get total and save into array
		    		$graphs['ios'][] = $ios_downloads[$d];
		    	} else {
		    		$graphs['ios'][] = 0;
		    	}

		    	//ios downloads
		    	if (isset($android_downloads[$d])) {
		    		//get total and save into array
		    		$graphs['android'][] = $android_downloads[$d];
		    	} else {
		    		$graphs['android'][] = 0;
		    	}
		    }
		}

		$data['graphs'] = json_encode($graphs);

		$data['sidebar_view'] = 'client/issey/sidebar';

		$this->load->view('client/issey/reports/graphs', $data);
	}

	public function graphs_hk() {
		$campaign_id = 2;
		$month = 7;
		$year = 2017;

		$start_date = date("Y-m-d", strtotime($year.'-'.$month.'-01'));
		$end_date = date("Y-m-t", strtotime($year.'-'.$month.'-01'));

		//no. of page visitor
		$this->db->select('DAY(date_created) AS DAY, COUNT(*) AS total');
		$this->db->like('logs', '"action":"visitor","page":"landing-hk"');
		$this->db->group_by('DAY(date_created)'); 
		$this->db->where('date_created >= "' . $start_date . ' 00:00:00" AND date_created <="' . $end_date . ' 11:59:59"' ); 
		$res1 = $this->db->get_where('h_campaign_log', array('campaign_id' => $campaign_id));

		//no. of registrants
		$this->db->select('DAY(date_created) AS DAY, COUNT(*) AS total');
		$this->db->like('logs', '"action":"register","page":"register-popup-hk"');
		$this->db->group_by('DAY(date_created)'); 
		$this->db->where('date_created >= "' . $start_date . ' 00:00:00" AND date_created <="' . $end_date . ' 11:59:59"' ); 
		$res2 = $this->db->get_where('h_campaign_log', array('campaign_id' => $campaign_id));

		//no. of ios downloads
		$this->db->select('DAY(date_created) AS DAY, COUNT(*) AS total');
		$this->db->like('logs', '"action":"download","page":"landing-hk\/ios"');
		$this->db->group_by('DAY(date_created)'); 
		$this->db->where('date_created >= "' . $start_date . ' 00:00:00" AND date_created <="' . $end_date . ' 11:59:59"' ); 
		$res3 = $this->db->get_where('h_campaign_log', array('campaign_id' => $campaign_id));

		//no. of android downloads
		$this->db->select('DAY(date_created) AS DAY, COUNT(*) AS total');
		$this->db->like('logs', '"action":"download","page":"landing-hk\/android"');
		$this->db->group_by('DAY(date_created)'); 
		$this->db->where('date_created >= "' . $start_date . ' 00:00:00" AND date_created <="' . $end_date . ' 11:59:59"' ); 
		$res4 = $this->db->get_where('h_campaign_log', array('campaign_id' => $campaign_id));

		//no. redeem android and ios
		$this->db->select('DAY(cvh.date_created) AS DAY, COUNT(*) AS total');
		$this->db->where('cvh.campaign_vouchers_id = cv.id AND cv.campaign_items_id = ci.id AND cvh.outlets_id = 3');
		$this->db->where('cvh.date_created >= "' . $start_date . ' 00:00:00" AND cvh.date_created <="' . $end_date . ' 11:59:59"' ); 
		$this->db->group_by('DAY(cvh.date_created)'); 
		$res5 = $this->db->get_where('h_campaign_vouchers AS cv, h_campaign_vouchers_history AS cvh, h_campaign_items as ci', 
			array(
				'campaign_id' => $campaign_id,
				'cvh.status' => 'consumed'
		));

		//no redeem android and ios
		$this->db->select('DAY(cvh.date_created) AS DAY, COUNT(*) AS total');
		$this->db->where('cvh.campaign_vouchers_id = cv.id AND cv.campaign_items_id = ci.id AND cvh.outlets_id = 4');
		$this->db->where('cvh.date_created >= "' . $start_date . ' 00:00:00" AND cvh.date_created <="' . $end_date . ' 11:59:59"' ); 
		$this->db->group_by('DAY(cvh.date_created)'); 
		$res6 = $this->db->get_where('h_campaign_vouchers AS cv, h_campaign_vouchers_history AS cvh, h_campaign_items as ci', 
			array(
				'campaign_id' => $campaign_id,
				'cvh.status' => 'consumed'
		));

		//no redeem android and ios
		$this->db->select('DAY(cvh.date_created) AS DAY, COUNT(*) AS total');
		$this->db->where('cvh.campaign_vouchers_id = cv.id AND cv.campaign_items_id = ci.id AND cvh.outlets_id = 5');
		$this->db->group_by('DAY(cvh.date_created)'); 
		$this->db->where('cvh.date_created >= "' . $start_date . ' 00:00:00" AND cvh.date_created <="' . $end_date . ' 11:59:59"' );
		$res7 = $this->db->get_where('h_campaign_vouchers AS cv, h_campaign_vouchers_history AS cvh, h_campaign_items as ci', 
			array(
				'campaign_id' => $campaign_id,
				'cvh.status' => 'consumed'
		));

		//no redeem android and ios
		$this->db->select('DAY(cvh.date_created) AS DAY, COUNT(*) AS total');
		$this->db->where('cvh.campaign_vouchers_id = cv.id AND cv.campaign_items_id = ci.id AND cvh.outlets_id = 6');
		$this->db->group_by('DAY(cvh.date_created)'); 
		$this->db->where('cvh.date_created >= "' . $start_date . ' 00:00:00" AND cvh.date_created <="' . $end_date . ' 11:59:59"' );
		$res8 = $this->db->get_where('h_campaign_vouchers AS cv, h_campaign_vouchers_history AS cvh, h_campaign_items as ci', 
			array(
				'campaign_id' => $campaign_id,
				'cvh.status' => 'consumed'
		));


		//no redeem android and ios
		$this->db->select('DAY(cvh.date_created) AS DAY, COUNT(*) AS total');
		$this->db->where('cvh.campaign_vouchers_id = cv.id AND cv.campaign_items_id = ci.id AND cvh.outlets_id = 7');
		$this->db->group_by('DAY(cvh.date_created)'); 
		$this->db->where('cvh.date_created >= "' . $start_date . ' 00:00:00" AND cvh.date_created <="' . $end_date . ' 11:59:59"' );
		$res9 = $this->db->get_where('h_campaign_vouchers AS cv, h_campaign_vouchers_history AS cvh, h_campaign_items as ci', 
			array(
				'campaign_id' => $campaign_id,
				'cvh.status' => 'consumed'
		));

		//no redeem android and ios
		$this->db->select('DAY(cvh.date_created) AS DAY, COUNT(*) AS total');
		$this->db->where('cvh.campaign_vouchers_id = cv.id AND cv.campaign_items_id = ci.id AND cvh.outlets_id = 8');
		$this->db->group_by('DAY(cvh.date_created)'); 
		$this->db->where('cvh.date_created >= "' . $start_date . ' 00:00:00" AND cvh.date_created <="' . $end_date . ' 11:59:59"' );
		$res10 = $this->db->get_where('h_campaign_vouchers AS cv, h_campaign_vouchers_history AS cvh, h_campaign_items as ci', 
			array(
				'campaign_id' => $campaign_id,
				'cvh.status' => 'consumed'
		));




		$graphs = array(
			'visitors' => array(),
			'registrants' => array(),
			'ios' => array(),
			'android' => array(),
			'outlet_3' => array(),
			'outlet_4' => array(),
			'outlet_5' => array(),
			'outlet_6' => array(),
			'outlet_7' => array(),
			'outlet_8' => array(),
			'labels' => array()
		);

		$visitor_result = array();
		$registrant_result = array();
		$ios_downloads = array();
		$android_downloads = array();
		$outlet_3 = array();
		$outlet_4 = array();
		$outlet_5 = array();
		$outlet_6 = array();
		$outlet_7 = array();
		$outlet_8 = array();

		//need to create an array to save result
		foreach ($res1->result_array() as $v) {
			$visitor_result[$v['DAY']] = $v['total'];
		}

		//need to create an array to save result
		foreach ($res2->result_array() as $v) {
			$registrant_result[$v['DAY']] = $v['total'];
		}

		//need to create an array to save result
		foreach ($res3->result_array() as $v) {
			$ios_downloads[$v['DAY']] = $v['total'];
		}

		//need to create an array to save result
		foreach ($res4->result_array() as $v) {
			$android_downloads[$v['DAY']] = $v['total'];
		}

		foreach ($res5->result_array() as $v) {
			$outlet_3[$v['DAY']] = $v['total'];
		}

		foreach ($res6->result_array() as $v) {
			$outlet_4[$v['DAY']] = $v['total'];
		}

		foreach ($res7->result_array() as $v) {
			$outlet_5[$v['DAY']] = $v['total'];
		}

		foreach ($res8->result_array() as $v) {
			$outlet_6[$v['DAY']] = $v['total'];
		}

		foreach ($res9->result_array() as $v) {
			$outlet_7[$v['DAY']] = $v['total'];
		}

		foreach ($res10->result_array() as $v) {
			$outlet_8[$v['DAY']] = $v['total'];
		}


		for($d = 1; $d <= 31; $d++)
		{
		    $time=mktime(12, 0, 0, $month, $d, $year);          
		    if (date('m', $time) == $month) {
		    	$graphs['labels'][] = date('M d, Y', $time);
		    
		    	//vistors
		    	if (isset($visitor_result[$d])) {
		    		//get total and save into array
		    		$graphs['visitors'][] = $visitor_result[$d];
		    	} else {
		    		$graphs['visitors'][] = 0;
		    	}

		    	//registrants
		    	if (isset($registrant_result[$d])) {
		    		//get total and save into array
		    		$graphs['registrants'][] = $registrant_result[$d];
		    	} else {
		    		$graphs['registrants'][] = 0;
		    	}

		    	//ios downloads
		    	if (isset($ios_downloads[$d])) {
		    		//get total and save into array
		    		$graphs['ios'][] = $ios_downloads[$d];
		    	} else {
		    		$graphs['ios'][] = 0;
		    	}
		    	//echo $android_downloads[$d];
		    	//ios downloads
		    	if (isset($android_downloads[$d])) {
		    		//get total and save into array
		    		$graphs['android'][] = $android_downloads[$d];
		    	} else {
		    		$graphs['android'][] = 0;
		    	}

		    	//outlet 3
		    	if (isset($outlet_3[$d])) {
		    		//get total and save into array
		    		$graphs['outlet_3'][] = $outlet_3[$d];
		    	} else {
		    		$graphs['outlet_3'][] = 0;
		    	}

		    	//outlet 4
		    	if (isset($outlet_4[$d])) {
		    		//get total and save into array
		    		$graphs['outlet_4'][] = $outlet_4[$d];
		    	} else {
		    		$graphs['outlet_4'][] = 0;
		    	}

		    	//outlet 5
		    	if (isset($outlet_5[$d])) {
		    		//get total and save into array
		    		$graphs['outlet_5'][] = $outlet_5[$d];
		    	} else {
		    		$graphs['outlet_5'][] = 0;
		    	}

		    	//outlet 6
		    	if (isset($outlet_6[$d])) {
		    		//get total and save into array
		    		$graphs['outlet_6'][] = $outlet_6[$d];
		    	} else {
		    		$graphs['outlet_6'][] = 0;
		    	}


		    	//outlet 7
		    	if (isset($outlet_7[$d])) {
		    		//get total and save into array
		    		$graphs['outlet_7'][] = $outlet_7[$d];
		    	} else {
		    		$graphs['outlet_7'][] = 0;
		    	}

		    	//outlet 8
		    	if (isset($outlet_8[$d])) {
		    		//get total and save into array
		    		$graphs['outlet_8'][] = $outlet_8[$d];
		    	} else {
		    		$graphs['outlet_8'][] = 0;
		    	}
		    }
		}

		$data['graphs'] = json_encode($graphs);

		$data['sidebar_view'] = 'client/issey/sidebar';

		$this->load->view('client/issey/reports/graphs_hk', $data);
	}

	public function export_registrants_hk() {
		$campaign_id = 2;

		//data with offset limit
		$this->db->select('id, uid, date_created');
		$this->db->like('logs', '"action":"register","page":"register-popup-hk"');
		$this->db->order_by('date_created DESC');
		$res2 = $this->db->get_where('h_campaign_log', array('campaign_id' => $campaign_id));

		$this->load->library('excel');

		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();

		// Set document properties
		$objPHPExcel->getProperties()->setCreator("h.@ctiv8")
									 ->setLastModifiedBy("h.@ctiv8")
									 ->setTitle("Customer Info");

		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);

		// Add some data
		$objPHPExcel->setActiveSheetIndex(0)
		            ->setCellValue('A1', 'ID')
		            ->setCellValue('B1', 'Info')
		            ->setCellValue('C1', 'Date Created');
		$row = 2;
		//re construct data show customer info
		foreach($res2->result_array() as $key => $customer) {
			$this->db->select('label, value');
			$cus_info = $this->db->get_where('h_client_customer_infos', array('client_customer_id' => $customer['uid']));

			$objPHPExcel->getActiveSheet()->setCellValue('A' . $row, $customer['id']);
			$_info = "";

			foreach ($cus_info->result_array() as $info) {
				$_info .= $info['label'] . " : " .$info['value']. " \n"; 
			}

			$objPHPExcel->getActiveSheet()->setCellValue('B' . $row, $_info);
			$objPHPExcel->getActiveSheet()->setCellValue('C' . $row, date('M d, Y g:i:s A', strtotime($customer['date_created'])));

			//alignment
			$objPHPExcel->getActiveSheet()->getStyle('B' . $row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP)->setWrapText(true);
			$objPHPExcel->getActiveSheet()->getStyle('C' . $row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP)->setWrapText(true);

			$row++;
		}

		// Rename worksheet
		$objPHPExcel->getActiveSheet()->setTitle('Customer Info');


		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);


		// Redirect output to a client’s web browser (Excel2007)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Report.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
		exit;
	}

	public function export_registrants() {
		$campaign_id = 2;

		//data with offset limit
		$this->db->select('id, uid, date_created');
		$this->db->like('logs', '"action":"register","page":"register-popup"');
		$this->db->order_by('date_created DESC');
		$res2 = $this->db->get_where('h_campaign_log', array('campaign_id' => $campaign_id));

		$this->load->library('excel');

		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();

		// Set document properties
		$objPHPExcel->getProperties()->setCreator("h.@ctiv8")
									 ->setLastModifiedBy("h.@ctiv8")
									 ->setTitle("Customer Info");

		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);

		// Add some data
		$objPHPExcel->setActiveSheetIndex(0)
		            ->setCellValue('A1', 'ID')
		            ->setCellValue('B1', 'Info')
		            ->setCellValue('C1', 'Date Created');
		$row = 2;
		//re construct data show customer info
		foreach($res2->result_array() as $key => $customer) {
			$this->db->select('label, value');
			$cus_info = $this->db->get_where('h_client_customer_infos', array('client_customer_id' => $customer['uid']));

			$objPHPExcel->getActiveSheet()->setCellValue('A' . $row, $customer['id']);
			$_info = "";

			foreach ($cus_info->result_array() as $info) {
				$_info .= $info['label'] . " : " .$info['value']. " \n"; 
			}

			$objPHPExcel->getActiveSheet()->setCellValue('B' . $row, $_info);
			$objPHPExcel->getActiveSheet()->setCellValue('C' . $row, date('M d, Y g:i:s A', strtotime($customer['date_created'])));

			//alignment
			$objPHPExcel->getActiveSheet()->getStyle('B' . $row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP)->setWrapText(true);
			$objPHPExcel->getActiveSheet()->getStyle('C' . $row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP)->setWrapText(true);

			$row++;
		}

		// Rename worksheet
		$objPHPExcel->getActiveSheet()->setTitle('Customer Info');


		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);


		// Redirect output to a client’s web browser (Excel2007)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Report.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
		exit;
	}


	private function _page_visitors() {
		$campaign_id = 2;

		//dataTables format
		$columns = array(
			0 => 'id',
			1 => 'date_created'
		);


		$offset = $_POST['start'];
		$limit = $_POST['length'];
		$order = $columns[$_POST['order'][0]['column']]. ' '.  $_POST['order'][0]['dir'];
		$draw = $_POST['draw'];

		//landing page visitors
		$this->db->select('COUNT(id) as total');
		$this->db->like('logs', '"action":"visitor","page":"landing"');
		$res = $this->db->get_where('h_campaign_log', array('campaign_id' => $campaign_id));

		//data with offset limit
		$this->db->select('id, date_created');
		$this->db->like('logs', '"action":"visitor","page":"landing"');
		$this->db->order_by($order);
		$res2 = $this->db->get_where('h_campaign_log', array('campaign_id' => $campaign_id), $limit, $offset);


		$data = array(
			'draw' => $draw,
			'recordsTotal' => $res->row()->total,
			'recordsFiltered' => $res->row()->total,
			'data' => $res2->result_array()
		);

		return $data;

	}

	private function _page_visitors_hk() {
		$campaign_id = 2;

		//dataTables format
		$columns = array(
			0 => 'id',
			1 => 'date_created'
		);


		$offset = $_POST['start'];
		$limit = $_POST['length'];
		$order = $columns[$_POST['order'][0]['column']]. ' '.  $_POST['order'][0]['dir'];
		$draw = $_POST['draw'];

		//landing page visitors
		$this->db->select('COUNT(id) as total');
		$this->db->like('logs', '"action":"visitor","page":"landing-hk"');
		$res = $this->db->get_where('h_campaign_log', array('campaign_id' => $campaign_id));

		//data with offset limit
		$this->db->select('id, date_created');
		$this->db->like('logs', '"action":"visitor","page":"landing-hk"');
		$this->db->order_by($order);
		$res2 = $this->db->get_where('h_campaign_log', array('campaign_id' => $campaign_id), $limit, $offset);


		$data = array(
			'draw' => $draw,
			'recordsTotal' => $res->row()->total,
			'recordsFiltered' => $res->row()->total,
			'data' => $res2->result_array()
		);

		return $data;

	}

	private function _registrants() {
		$campaign_id = 2;

		//dataTables format
		$columns = array(
			0 => 'id',
			1 => 'info',
			2 => 'date_created'
		);


		$offset = $_POST['start'];
		$limit = $_POST['length'];
		$order = $columns[$_POST['order'][0]['column']]. ' '.  $_POST['order'][0]['dir'];
		$draw = $_POST['draw'];

		//landing page visitors
		$this->db->select('COUNT(id) as total');
		$this->db->like('logs', '"action":"register","page":"register-popup"');
		$res = $this->db->get_where('h_campaign_log', array('campaign_id' => $campaign_id));

		//data with offset limit
		$this->db->select('id, uid, date_created');
		$this->db->like('logs', '"action":"register","page":"register-popup"');
		$this->db->order_by($order);
		$res2 = $this->db->get_where('h_campaign_log', array('campaign_id' => $campaign_id), $limit, $offset);

		$result_data =array();

		//re construct data show customer info
		foreach($res2->result_array() as $key => $customer) {
			$this->db->select('label, value');
			$cus_info = $this->db->get_where('h_client_customer_infos', array('client_customer_id' => $customer['uid']));

			$result_data[$key]['id'] = $customer['id'];
			$result_data[$key]['date_created'] = $customer['date_created'];
			$result_data[$key]['info'] = '';

			foreach ($cus_info->result_array() as $info) {
				$result_data[$key]['info'] .= $info['label'] . ' : ' .$info['value']. '<br />'; 
			}
		}


		$data = array(
			'draw' => $draw,
			'recordsTotal' => $res->row()->total,
			'recordsFiltered' => $res->row()->total,
			'data' => $result_data
		);

		return $data;

	}

	private function _registrants_hk() {
		$campaign_id = 2;

		//dataTables format
		$columns = array(
			0 => 'id',
			1 => 'info',
			2 => 'date_created'
		);


		$offset = $_POST['start'];
		$limit = $_POST['length'];
		$order = $columns[$_POST['order'][0]['column']]. ' '.  $_POST['order'][0]['dir'];
		$draw = $_POST['draw'];

		//landing page visitors
		$this->db->select('COUNT(id) as total');
		$this->db->like('logs', '"action":"register","page":"register-popup-hk"');
		$res = $this->db->get_where('h_campaign_log', array('campaign_id' => $campaign_id));

		//data with offset limit
		$this->db->select('id, uid, date_created');
		$this->db->like('logs', '"action":"register","page":"register-popup-hk"');
		$this->db->order_by($order);
		$res2 = $this->db->get_where('h_campaign_log', array('campaign_id' => $campaign_id), $limit, $offset);

		$result_data =array();

		//re construct data show customer info
		foreach($res2->result_array() as $key => $customer) {
			$this->db->select('label, value');
			$cus_info = $this->db->get_where('h_client_customer_infos', array('client_customer_id' => $customer['uid']));

			$result_data[$key]['id'] = $customer['id'];
			$result_data[$key]['date_created'] = $customer['date_created'];
			$result_data[$key]['info'] = '';

			foreach ($cus_info->result_array() as $info) {
				$result_data[$key]['info'] .= $info['label'] . ' : ' .$info['value']. '<br />'; 
			}
		}


		$data = array(
			'draw' => $draw,
			'recordsTotal' => $res->row()->total,
			'recordsFiltered' => $res->row()->total,
			'data' => $result_data
		);

		return $data;

	}

	private function _download_from_android() {
		$campaign_id = 2;

		//dataTables format
		$columns = array(
			0 => 'id',
			1 => 'date_created'
		);


		$offset = $_POST['start'];
		$limit = $_POST['length'];
		$order = $columns[$_POST['order'][0]['column']]. ' '.  $_POST['order'][0]['dir'];
		$draw = $_POST['draw'];

		//no. of download voucher android
		$this->db->select('COUNT(*) AS total');
		$this->db->like('logs', '"action":"download","page":"landing\/android"');
		$res = $this->db->get_where('h_campaign_log', array('campaign_id' => $campaign_id));

		//data with offset limit
		$this->db->select('id, date_created');
		$this->db->like('logs', '"action":"download","page":"landing\/android"');
		$this->db->order_by($order);
		$res2 = $this->db->get_where('h_campaign_log', array('campaign_id' => $campaign_id), $limit, $offset);


		$data = array(
			'draw' => $draw,
			'recordsTotal' => $res->row()->total,
			'recordsFiltered' => $res->row()->total,
			'data' => $res2->result_array()
		);

		return $data;

	}

	private function _download_from_android_hk() {
		$campaign_id = 2;

		//dataTables format
		$columns = array(
			0 => 'id',
			1 => 'date_created'
		);


		$offset = $_POST['start'];
		$limit = $_POST['length'];
		$order = $columns[$_POST['order'][0]['column']]. ' '.  $_POST['order'][0]['dir'];
		$draw = $_POST['draw'];

		//no. of download voucher android
		$this->db->select('COUNT(*) AS total');
		$this->db->like('logs', '"action":"download","page":"landing-hk\/android"');
		$res = $this->db->get_where('h_campaign_log', array('campaign_id' => $campaign_id));

		//data with offset limit
		$this->db->select('id, date_created');
		$this->db->like('logs', '"action":"download","page":"landing-hk\/android"');
		$this->db->order_by($order);
		$res2 = $this->db->get_where('h_campaign_log', array('campaign_id' => $campaign_id), $limit, $offset);


		$data = array(
			'draw' => $draw,
			'recordsTotal' => $res->row()->total,
			'recordsFiltered' => $res->row()->total,
			'data' => $res2->result_array()
		);

		return $data;

	}


	private function _download_from_ios() {
		$campaign_id = 2;
		//dataTables format
		$columns = array(
			0 => 'id',
			1 => 'date_created'
		);


		$offset = $_POST['start'];
		$limit = $_POST['length'];
		$order = $columns[$_POST['order'][0]['column']]. ' '.  $_POST['order'][0]['dir'];
		$draw = $_POST['draw'];

		//no. of download voucher ios
		$this->db->select('COUNT(*) AS total');
		$this->db->like('logs', '"action":"download","page":"landing\/ios"');
		$res = $this->db->get_where('h_campaign_log', array('campaign_id' => $campaign_id));

		//data with offset limit
		$this->db->select('id, date_created');
		$this->db->like('logs', '"action":"download","page":"landing\/ios"');
		$this->db->order_by($order);
		$res2 = $this->db->get_where('h_campaign_log', array('campaign_id' => $campaign_id), $limit, $offset);


		$data = array(
			'draw' => $draw,
			'recordsTotal' => $res->row()->total,
			'recordsFiltered' => $res->row()->total,
			'data' => $res2->result_array()
		);

		return $data;

	}

	private function _download_from_ios_hk() {
		$campaign_id = 2;
		//dataTables format
		$columns = array(
			0 => 'id',
			1 => 'date_created'
		);


		$offset = $_POST['start'];
		$limit = $_POST['length'];
		$order = $columns[$_POST['order'][0]['column']]. ' '.  $_POST['order'][0]['dir'];
		$draw = $_POST['draw'];

		//no. of download voucher ios
		$this->db->select('COUNT(*) AS total');
		$this->db->like('logs', '"action":"download","page":"landing-hk\/ios"');
		$res = $this->db->get_where('h_campaign_log', array('campaign_id' => $campaign_id));

		//data with offset limit
		$this->db->select('id, date_created');
		$this->db->like('logs', '"action":"download","page":"landing-hk\/ios"');
		$this->db->order_by($order);
		$res2 = $this->db->get_where('h_campaign_log', array('campaign_id' => $campaign_id), $limit, $offset);


		$data = array(
			'draw' => $draw,
			'recordsTotal' => $res->row()->total,
			'recordsFiltered' => $res->row()->total,
			'data' => $res2->result_array()
		);

		return $data;

	}


	private function _redemptions($channel) {
		//dataTables format
		$columns = array(
			0 => 'cv.id',
			1 => 'cvh.date_created'
		);


		$offset = $_POST['start'];
		$limit = $_POST['length'];
		$order = $columns[$_POST['order'][0]['column']]. ' '.  $_POST['order'][0]['dir'];
		$draw = $_POST['draw'];

		//no of redeem ios
		$this->db->select('COUNT(*) AS total');
		$this->db->where('cvh.campaign_vouchers_id = cv.id AND cv.campaign_items_id = ci.id AND cvh.outlets_id NOT IN ('.$this->hk_outlets.')');
		$res = $this->db->get_where('h_campaign_vouchers AS cv, h_campaign_vouchers_history AS cvh, h_campaign_items as ci', 
			array(
				'ci.campaign_id' => 2,
				'cvh.status' => 'consumed',
				'cv.channel' => $channel,
		));

		//data with offset limit
		$this->db->select('cv.id, cvh.date_created');
		$this->db->where('cvh.campaign_vouchers_id = cv.id AND cv.campaign_items_id = ci.id AND cvh.outlets_id NOT IN ('.$this->hk_outlets.')');
		$this->db->order_by($order);
		$res2 = $this->db->get_where('h_campaign_vouchers AS cv, h_campaign_vouchers_history AS cvh, h_campaign_items as ci', 
			array(
				'ci.campaign_id' => 2,
				'cvh.status' => 'consumed',
				'cv.channel' => $channel,
		));


		$data = array(
			'draw' => $draw,
			'recordsTotal' => $res->row()->total,
			'recordsFiltered' => $res->row()->total,
			'data' => $res2->result_array()
		);

		return $data;

	}

	private function _redemptions_hk($channel) {
		//dataTables format
		$columns = array(
			0 => 'cv.id',
			1 => 'cvh.date_created'
		);


		$offset = $_POST['start'];
		$limit = $_POST['length'];
		$order = $columns[$_POST['order'][0]['column']]. ' '.  $_POST['order'][0]['dir'];
		$draw = $_POST['draw'];

		//no of redeem ios
		$this->db->select('COUNT(*) AS total');
		$this->db->where('cvh.campaign_vouchers_id = cv.id AND cv.campaign_items_id = ci.id AND cvh.outlets_id IN ('.$this->hk_outlets.')');
		$res = $this->db->get_where('h_campaign_vouchers AS cv, h_campaign_vouchers_history AS cvh, h_campaign_items as ci', 
			array(
				'ci.campaign_id' => 2,
				'cvh.status' => 'consumed',
				'cv.channel' => $channel,
		));

		//data with offset limit
		$this->db->select('cv.id, cvh.date_created');
		$this->db->where('cvh.campaign_vouchers_id = cv.id AND cv.campaign_items_id = ci.id AND cvh.outlets_id IN ('.$this->hk_outlets.')');
		$this->db->order_by($order);
		$res2 = $this->db->get_where('h_campaign_vouchers AS cv, h_campaign_vouchers_history AS cvh, h_campaign_items as ci', 
			array(
				'ci.campaign_id' => 2,
				'cvh.status' => 'consumed',
				'cv.channel' => $channel,
		));


		$data = array(
			'draw' => $draw,
			'recordsTotal' => $res->row()->total,
			'recordsFiltered' => $res->row()->total,
			'data' => $res2->result_array()
		);

		return $data;

	}
}
