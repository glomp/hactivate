<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{	
		$is_log = $this->session->userdata('logged_in');

		if ($is_log == NULL && $is_log == FALSE) {
            return redirect(base_url('client/login'));
        }

        $data['sidebar_view'] = 'client/martell/sidebar'; 

		//no. of page visitors
		//no download voucher ios
		//no. download voucher android
		//no. of redemption android
		//no. of redemption android

		//no. of download voucher ios
		$this->db->select('COUNT(*) AS total');
		$this->db->like('logs', '"action":"download","page":"redeem\/ios"');
		$res1 = $this->db->get_where('h_campaign_log', array('campaign_id' => 1));

		//no. of download voucher android
		$this->db->select('COUNT(*) AS total');
		$this->db->like('logs', '"action":"download","page":"redeem\/bookmark"');
		$res2 = $this->db->get_where('h_campaign_log', array('campaign_id' => 1));

		//no. of page visitor
		$this->db->select('COUNT(*) AS total');
		$this->db->like('logs', '"action":"visitor","page":"landing"');
		$res3 = $this->db->get_where('h_campaign_log', array('campaign_id' => 1));

		//no of redeem ios
		$this->db->select('COUNT(*) AS total');
		$this->db->where('cvh.campaign_vouchers_id = cv.id AND cv.campaign_items_id = ci.id');
		$res4 = $this->db->get_where('h_campaign_vouchers AS cv, h_campaign_vouchers_history AS cvh, h_campaign_items as ci', 
			array(
				'campaign_id' => 1,
				'cvh.status' => 'consumed',
				'cv.channel' => 'ios',
		));

		//no redeem android
		$this->db->select('COUNT(*) AS total');
		$this->db->where('cvh.campaign_vouchers_id = cv.id AND cv.campaign_items_id = ci.id');
		$res5 = $this->db->get_where('h_campaign_vouchers AS cv, h_campaign_vouchers_history AS cvh, h_campaign_items as ci', 
			array(
				'campaign_id' => 1,
				'cvh.status' => 'consumed',
				'cv.channel' => 'web',
		));

		$data['total_ios_download'] = $res1->row()->total;
		$data['total_android_download'] = $res2->row()->total;
		$data['total_page_visitor'] = $res3->row()->total;
		$data['total_redeemed_ios'] = $res4->row()->total;
		$data['total_redeemed_android'] = $res5->row()->total;

		$this->load->view('client/martell/index', $data);
	}
}
