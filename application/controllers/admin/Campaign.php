<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Campaign extends CI_Controller {

	
	public function index()
	{
		$data['campaigns'] = array();
		$query = $this->db->get('h_campaigns');
	   
	   if($query->num_rows() > 0)
	   {
	       $data['campaigns'] = $query->result();
	   }

		$this->load->view('admin/campaign/list',  $data);
	}

	


}
