<?php

function generate_hash($length = 8) {
    $chars = 'abcdefghijkmnpqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ23456789';
    $count = mb_strlen($chars);

    for ($i = 0, $result = ''; $i < $length; $i++) {
        $index = rand(0, $count - 1);
        $result .= mb_substr($chars, $index, 1);
    }
    return $result;
}

function hash_key_value($salt, $password) {
    $hash_password = hash('SHA256', $salt . $password);
    return $hash_password;
}