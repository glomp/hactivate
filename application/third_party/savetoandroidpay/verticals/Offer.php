<?php
/**
 * Copyright 2013 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Generates an example Offer Class and Object.
 */
class Offer {

  /**
   * Create an example Offer Object
   *
   * @param String $issuerId Wallet Object merchant account id.
   * @param String $classId Wallet Class that this wallet object references.
   * @param String $objectId Unique identifier for a wallet object.
   * @return Object $wobObject Offerobject resource.
   */
  public static function generateOfferObject($issuerId, $classId, $objectId, $url) {

          
    // Define text module data.
    $textModulesData = array(
        array(
            'header' => '',
            'body' => 'Click below to redeem.'
        )
    );
    
    // Define links module data.
    $linksModuleData = new Google_Service_Walletobjects_LinksModuleData();
    $uris = array (
        array(
            'uri' => $url,
            'kind' => 'walletobjects#uri',
            'description' => 'REDEEM NOW'
        )
    );
    $linksModuleData->setUris($uris);
    
    // Define info module data.
    $infoModuleData = new Google_Service_Walletobjects_InfoModuleData();
    $infoModuleData->setHexBackgroundColor('#442905');
    $infoModuleData->setHexFontColor('#F8EDC1');
    $infoModuleData->setShowLastUpdateTime(true);
    // Messages to be displayed.
    $messages = array(
        array(
            'actionUri' => array(
            'kind' => 'walletobjects#uri',
            'uri' => 'http://www.isseymiyakeparfums.com/en'
            ),
            'header' => '',
            'body' => 'Present this e-voucher from the nearest Issey Miyake counter.  Whilst stocks last!',
            'kind' => 'walletobjects#walletObjectMessage'
        ),
        array(
            'actionUri' => array(
            'kind' => 'walletobjects#uri',
            'uri' => 'http://www.isseymiyakeparfums.com/en'
            ),
            'header' => '',
            'body' => 'Experience a suspended moment with L’eau d’Issey Pure eau de toilette.',
            'kind' => 'walletobjects#walletObjectMessage'
        )
    );
      
    // Create wallet object.
    $wobObject = new Google_Service_Walletobjects_OfferObject();
    $wobObject->setClassId($issuerId.'.'.$classId);
    $wobObject->setId($issuerId.'.'.$objectId);
    $wobObject->setState('active');
    $wobObject->setVersion(1);
    $wobObject->setInfoModuleData($infoModuleData);
    $wobObject->setLinksModuleData($linksModuleData);
    $wobObject->setTextModulesData($textModulesData);
    $wobObject->setMessages($messages);
    $wobObject->setKind('walletobjects#offerObject');
    return $wobObject;
  }

  public static function generateDemoOfferObject($issuerId, $classId, $objectId, $url) {
       // Define text module data.
        $textModulesData = array(
            array(
                'header' => 'This is Header 1 of a text module and has 502 characters Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                'body' => 'This is Body 1 of a text module and has 500 characters Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
            ),
            array(
                'header' => 'This is Header 2 of a text module and has 55 characters',
                'body' => 'This is Body 2 of a text module and has 53 characters'
            ),
            array(
                'header' => 'This is Header 3 of a text module and has 55 characters',
                'body' => 'This is Body 3 of a text module and has 53 characters'
            ),
            array(
                'header' => 'This is Header 4 of a text module and has 55 characters',
                'body' => 'This is Body 4 of a text module and has 53 characters'
            )
        );
        
        // Define links module data.
        $linksModuleData = new Google_Service_Walletobjects_LinksModuleData();
        $uris = array (
            array(
                'uri' => 'https://www.google.com/maps/@14.3824813,120.976396,19z',
                'kind' => 'walletobjecs#uri',
                'description' => 'This is Nearby Locations button and it has 503 characters Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
            ),
            array(
                'uri' => 'tel:+630465199445',
                'kind' => 'walletobjects#uri',
                'description' => 'This is Call button it has 40 characters'
            ),
            array(
                'uri' => $url,
                'kind' => 'walletobjects#uri',
                'description' => 'This is Redeem now button 1 it has 48 characters'
            ),
            array(
                'uri' => $url,
                'kind' => 'walletobjects#uri',
                'description' => 'This is Redeem now button 2 it has 48 characters'
            ),
            array(
                'uri' => $url,
                'kind' => 'walletobjects#uri',
                'description' => 'This is Redeem now button 3 it has 48 characters'
            )
            
        );
        $linksModuleData->setUris($uris);
        
        // Define info module data.
        $infoModuleData = new Google_Service_Walletobjects_InfoModuleData();
        $infoModuleData->setHexBackgroundColor('#c0c0c0');
        $infoModuleData->setHexFontColor('#FF0000');
        $infoModuleData->setShowLastUpdateTime(true);
        // Messages to be displayed.
        $messages = array(
            array(
                'actionUri' => array(
                'kind' => 'walletobjects#uri',
                'uri' => 'http://staging2.glomp.it/hactivate/landing/page/demo'
                ),
                'header' => 'This is Header Message 1 of a message and has 506 characters Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                'body' => 'This is Body Message 1 of a message and has 504 characters Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                'kind' => 'walletobjects#walletObjectMessage'
            ),
            array(
                'actionUri' => array(
                'kind' => 'walletobjects#uri',
                'uri' => 'http://staging2.glomp.it/hactivate/landing/page/demo'
                ),
                'header' => 'This is Header Message 1 of a message and has 59 characters',
                'body' => 'This is Body Message 1 of a message and has 57 characters',
                'kind' => 'walletobjects#walletObjectMessage'
            ),
            array(
                'actionUri' => array(
                'kind' => 'walletobjects#uri',
                'uri' => 'http://staging2.glomp.it/hactivate/landing/page/demo'
                ),
                'header' => 'This is Header Message 1 of a message and has 59 characters',
                'body' => 'This is Body Message 1 of a message and has 57 characters',
                'kind' => 'walletobjects#walletObjectMessage'
            )
        );
          
        // Create wallet object.
//        $wobObject = new Google_Service_Walletobjects_OfferObject();
//        $wobObject->setClassId($issuerId.'.'.$classId);
//        $wobObject->setId($issuerId.'.'.$objectId);
//        $wobObject->setState('active');
//        $wobObject->setVersion(1);
//        $wobObject->setInfoModuleData($infoModuleData);
//        $wobObject->setLinksModuleData($linksModuleData);
//        $wobObject->setTextModulesData($textModulesData);
//        $wobObject->setMessages($messages);
//        $wobObject->setKind('walletobjects#offerObject');
      
      
        $barcode = new Google_Service_Walletobjects_Barcode();
        $barcode->setType('upcA');
        $barcode->setValue('123456789012');
        $barcode->setAlternateText('126Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.');

        // VALID TIME INTERVALS
//        $validTimeInterval = new Google_TimeInterval();
//        $startDateTime = new Google_DateTime();
//        $startDateTime->setDate('2013-06-12T23:20:50.52Z');
//        $validTimeInterval->setStart($startDateTime);
//        $endDateTime = new Google_DateTime();
//        $endDateTime->setDate('2013-12-12T23:20:50.52Z');
//        $validTimeInterval->setEnd($endDateTime);

        // Create wallet object.
        $offerObject = new Google_Service_Walletobjects_OfferObject();
        $offerObject->setClassId($issuerId.'.'.$classId);
        $offerObject->setId($issuerId.'.'.$objectId);
        $offerObject->setState('active');
        $offerObject->setVersion(1);
        $offerObject->setBarcode($barcode);
//        $offerObject->setValidTimeInterval($validTimeInterval);
        $offerObject->setInfoModuleData($infoModuleData);
        $offerObject->setLinksModuleData($linksModuleData);
        $offerObject->setTextModulesData($textModulesData);
        $offerObject->setMessages($messages);
        $offerObject->setKind('walletobjects#offerObject');
      
      
        return $offerObject;
    }

    public static function generateOfferObjectHk($issuerId, $classId, $objectId, $url) {
        // Define text module data.
        $textModulesData = array(
            array(
                'header' => 'apm Pop-up Store',
                'body' => '觀塘觀塘道418號apm地下 (近通往LG之扶手電梯)'
            ),
            array(
                'header' => '澳門新八佰伴',
                'body' => '澳門蘇亞利斯博士大馬路一樓1-17'
            ),
            array(
                'header' => 'Beauty Avenue朗豪坊',
                'body' => '九龍旺角朗豪坊一樓Beauty Avenue'
            ),
            array(
                'header' => '崇光銅鑼灣店',
                'body' => '銅鑼灣崇光百貨B1'
            ),
            array(
                'header' => '尖沙咀海運大廈FACESSS',
                'body' => '尖沙咀廣東道3號海運大廈二樓FACESSS'
            ),
            array(
                'header' => '連卡佛 (廣東道)',
                'body' => '尖沙咀廣東道3號海運大廈二樓連卡佛'
            ),
            array(
                'header' => '',
                'body' => '按下列按鈕以換領'
            )
        );

        // Define links module data.
        $linksModuleData = new Google_Service_Walletobjects_LinksModuleData();
        $uris = array (
            array(
                'uri' => $url,
                'kind' => 'walletobjecs#uri',
                'description' => '立即換領'
            )
        );
        $linksModuleData->setUris($uris);

        // Define info module data.
        $infoModuleData = new Google_Service_Walletobjects_InfoModuleData();
        $infoModuleData->setHexBackgroundColor('#442905');
        $infoModuleData->setHexFontColor('#F8EDC1');
        $infoModuleData->setShowLastUpdateTime(true);
        // Messages to be displayed.
        $messages = array(
            array(
                'actionUri' => array(
                'kind' => 'walletobjects#uri',
                'uri' => 'http://www.isseymiyakeparfums.com/en'
                ),
                'header' => '',
                'body' => '出示此電子換領券，並到您所選擇之香氛專櫃，免費領取試用裝一份。數量有限，換完即止。',
                'kind' => 'walletobjects#walletObjectMessage'
            ),
            array(
                'actionUri' => array(
                'kind' => 'walletobjects#uri',
                'uri' => 'http://www.isseymiyakeparfums.com/en'
                ),
                'header' => '',
                'body' => '體驗全新L’Eau d’Issey Pure淡香氛為您帶來的寧靜時刻。',
                'kind' => 'walletobjects#walletObjectMessage'
            )
        );

        // Create wallet object.
        $wobObject = new Google_Service_Walletobjects_OfferObject();
        $wobObject->setClassId($issuerId.'.'.$classId);
        $wobObject->setId($issuerId.'.'.$objectId);
        $wobObject->setState('active');
        $wobObject->setVersion(1);
        $wobObject->setInfoModuleData($infoModuleData);
        $wobObject->setLinksModuleData($linksModuleData);
        $wobObject->setTextModulesData($textModulesData);
        $wobObject->setMessages($messages);
        $wobObject->setKind('walletobjects#offerObject');
        return $wobObject;
  }
    
  public static function generateBodyShopOfferObject($issuerId, $classId, $objectId, $url) {
       // Define text module data.
//        $textModulesData = array(
//            array(
//                'header' => '',
//                'body' => 'Voucher is valid till 25 Dec 2017 at all The Body Shop stores in Singapore except IMM and not valid with any other promotions. Voucher is limited to 1 redemption per customer. While stocks last.'
//            ),
//            array(
//                'header' => '',
//                'body' => 'Do not click ‘REDEEM NOW’ until you’re at the store presenting this voucher to the store staff'
//            )
//        );
        
        // Define links module data.
        $linksModuleData = new Google_Service_Walletobjects_LinksModuleData();
        $uris = array (
            array(
                'uri' => $url,
                'kind' => 'walletobjects#uri',
                'description' => 'REDEEM NOW'
            )
        );
        $linksModuleData->setUris($uris);
        
        // Define info module data.
        $infoModuleData = new Google_Service_Walletobjects_InfoModuleData();
        $infoModuleData->setHexBackgroundColor('#00584b');
        $infoModuleData->setHexFontColor('#F5F500');
        $infoModuleData->setShowLastUpdateTime(true);
        // Messages to be displayed.
        $messages = array(
            array(
                'actionUri' => 
                array(
                    'kind' => 'walletobjects#uri',
                    'uri' => 'https://thebodyshop.com'
                ),
                    'header' => '',
                    'body' => 'Present this voucher in stores and redeem our Christmas Limited Edition Duck n’ Dive gift set at just $10 (U.P. $22)!

Voucher is valid till 25 Dec 2017 at all The Body Shop stores except IMM and Takashimaya Basement 1.
Voucher is not valid with any other promotions. Voucher is limited to 1 redemption per customer.

While stocks last.

Do not click \'Redeem\' until you\'re at the store presenting this voucher to the store staff',
                    'kind' => 'walletobjects#walletObjectMessage'
                )
        );
          
        // Create wallet object.
        $wobObject = new Google_Service_Walletobjects_OfferObject();
        $wobObject->setClassId($issuerId.'.'.$classId);
        $wobObject->setId($issuerId.'.'.$objectId);
        $wobObject->setState('active');
        $wobObject->setVersion(1);
        $wobObject->setInfoModuleData($infoModuleData);
        $wobObject->setLinksModuleData($linksModuleData);
//        $wobObject->setTextModulesData($textModulesData);
        $wobObject->setMessages($messages);
        $wobObject->setKind('walletobjects#offerObject');
        return $wobObject;
    }
    
    public static function generateBodyShopOfferTruthOrDareObject($issuerId, $classId, $objectId, $url) {
              // Define text module data.
//        $textModulesData = array(
//            array(
//                'header' => '',
//                'body' => 'Voucher is valid till 25 Dec 2017 at all The Body Shop stores in Singapore except IMM and not valid with any other promotions. Voucher is limited to 1 redemption per customer. While stocks last.'
//            ),
//            array(
//                'header' => '',
//                'body' => 'Do not click ‘REDEEM NOW’ until you’re at the store presenting this voucher to the store staff'
//            )
//        );
        
        // Define links module data.
        $linksModuleData = new Google_Service_Walletobjects_LinksModuleData();
        $uris = array (
            array(
                'uri' => $url,
                'kind' => 'walletobjects#uri',
                'description' => 'REDEEM NOW'
            )
        );
        $linksModuleData->setUris($uris);
        
        // Define info module data.
        $infoModuleData = new Google_Service_Walletobjects_InfoModuleData();
        $infoModuleData->setHexBackgroundColor('#00584b');
        $infoModuleData->setHexFontColor('#F5F500');
        $infoModuleData->setShowLastUpdateTime(true);
        // Messages to be displayed.
        $messages = array(
            array(
                'actionUri' => 
                array(
                    'kind' => 'walletobjects#uri',
                    'uri' => 'https://thebodyshop.com'
                ),
                    'header' => '',
                    'body' => 'Present this voucher in stores and redeem our Christmas Limited Edition Truth Or Dare gift set at just $10 (U.P. $20)!

Voucher is valid till 25 Dec 2017 at all The Body Shop stores except IMM and Takashimaya Basement 1.

Voucher is not valid with any other promotions. Voucher is limited to 1 redemption per customer.

While stocks last.

Do not click \'Redeem\' until you\'re at the store presenting this voucher to the store staff.',
                    'kind' => 'walletobjects#walletObjectMessage'
                )
        );
          
        // Create wallet object.
        $wobObject = new Google_Service_Walletobjects_OfferObject();
        $wobObject->setClassId($issuerId.'.'.$classId);
        $wobObject->setId($issuerId.'.'.$objectId);
        $wobObject->setState('active');
        $wobObject->setVersion(1);
        $wobObject->setInfoModuleData($infoModuleData);
        $wobObject->setLinksModuleData($linksModuleData);
//        $wobObject->setTextModulesData($textModulesData);
        $wobObject->setMessages($messages);
        $wobObject->setKind('walletobjects#offerObject');
        return $wobObject;
    }
    
    public static function generateCarlsbergOfferObject($issuerId, $classId, $objectId, $url) {
        //Define text module data.
        $textModulesData = array(
            array(
                'header' => '',
                'body' => 'Please press Redeem button in front of bar / restaurant staffs
請於到達餐廳/酒吧後，於店員面前按下Redeem。'
            )
        );
        
        // Define links module data.
        $linksModuleData = new Google_Service_Walletobjects_LinksModuleData();
        $uris = array (
            array(
                'uri' => $url,
                'kind' => 'walletobjects#uri',
                'description' => 'REDEEM NOW'
            )
        );
        $linksModuleData->setUris($uris);
        
        // Define info module data.
        $infoModuleData = new Google_Service_Walletobjects_InfoModuleData();
        $infoModuleData->setHexBackgroundColor('#00584b');
        $infoModuleData->setHexFontColor('#F5F500');
        $infoModuleData->setShowLastUpdateTime(true);
        // Messages to be displayed.
        $messages = array(
            array(
                'actionUri' => 
 
                array(
                    'kind' => 'walletobjects#uri',
                    'uri' => 'https://carlsberg.com'
                ),
                    'header' => '使用之條款及細則：',
                    'body' => '1. 優惠期: 2018年2月9至23日
2. 此券適用於上述指定商店。
3. 此券只可使用一次。此券可兌換嘉士伯生啤買一送一優惠以及額外以半價享用指定食物一客。
4. 請於點菜前出示此券；影印本或屏幕載圖恕不接受。
5. 此券不可兌換現金或轉讓，亦不設找贖。
6. 如欲停止接收通知提示，請刪除優惠卷。
7. 此券可與Happy Hour優惠同時使用，但不可與其他信用卡或員工優惠同時使用。
8. 如有任何爭議，一概以嘉士伯香港有限公司的最終決定為準。
9. 如有任何查詢，請致電 3189 8222 / 3189 8224。',
                    'kind' => 'walletobjects#walletObjectMessage'
                ),
            array(
                'actionUri' => 
                array(
                    'kind' => 'walletobjects#uri',
                    'uri' => 'https://carlsberg.com'
                ),
                    'header' => 'Terms & Conditions:',
                    'body' => '1. Promotion period: 9 – 23 February, 2018
2. This coupon can be used in above outlets.
3. This coupon can redeem Carlsberg draught beer buy 1 get 1 free offer and selected food at 50% off once only.
4. Please present this coupon before ordering. Photocopies and screen shot will not be accepted.
5. This coupon is not transferable, cash redeemable or refundable.
6. Consumer who wants to opt out from receiving notification, please delete the coupon.
7. This coupon can be used in conjunction with the Happy Hour offers but not with any other credit card or staff offers.
9. Carlsberg Hong Kong Limited reserves the right to make the final decision in the event of any dispute.
10. Enquiry hotline: 3189 8222 / 3189 8224.',
                    'kind' => 'walletobjects#walletObjectMessage'
                ),
            array(
                'actionUri' => 
                array(
                    'kind' => 'walletobjects#uri',
                    'uri' => 'https://carlsberg.com'
                ),
                    'header' => 'Redemption Locations',
                    'body' => '1. Coyote Discovery Bay: Shop G08A, G/F, D\'Deck, Discovery Bay, Lantau Island / Tel: 2987 2848
2. Coyote Wanchai: G/F, Gaylord Commercial Building, 114-120 Lockhart Road, Wan Chai, HK / Tel: 2861 2221
3. Grand Central: Shop R001, Roof Level, Civic Square, Elements, 1 Austin Road West, Tsim Sha Tsui, KLN / Tel: 2736 4888
4. McSorley’s Discovery Bay: Block B, G/F, D’Deck, Discovery Bay, Lantau Island / Tel: 2987 8280
5. McSorley’s Soho: G/F, 46 Staunton Street, Soho, Central, HK / Tel: 2522 2646
6. The Jockey: 33 Wong Nai Chung Road, Happy Valley, HK / Tel: 2572 2266
7. Wildfire Fashion Walk: Shop C-D, G/F, 59-65 Paterson Street, Causeway Bay, HK / Tel: 2352 0198
8. Wildfire Knutsford Terrace: G/F, 2 Knutsford Terrace, Tsim Sha Tsui, KLN / Tel: 3690 1598
9. Wildfire The Peak: Shop 2, 1/F, The Peak Tower, 128 Peak Road, The Peak, HK / Tel: 2849 5123',
                    'kind' => 'walletobjects#walletObjectMessage'
                )
            
        );
          
        // Create wallet object.
        $wobObject = new Google_Service_Walletobjects_OfferObject();
        $wobObject->setClassId($issuerId.'.'.$classId);
        $wobObject->setId($issuerId.'.'.$objectId);
        $wobObject->setState('active');
        $wobObject->setVersion(1);
        $wobObject->setInfoModuleData($infoModuleData);
        $wobObject->setLinksModuleData($linksModuleData);
        $wobObject->setTextModulesData($textModulesData);
        $wobObject->setMessages($messages);
        $wobObject->setKind('walletobjects#offerObject');
        return $wobObject;
    }
    
    public static function generateGenericOfferObject($issuerId, $classId, $objectId, $url) {
        
        // Define links module data.
        $linksModuleData = new Google_Service_Walletobjects_LinksModuleData();
        $uris = array (
            array(
                'uri' => $url,
                'kind' => 'walletobjects#uri',
                'description' => 'REDEEM NOW'
            )
        );
        $linksModuleData->setUris($uris);
            
        // Define info module data.
        $infoModuleData = new Google_Service_Walletobjects_InfoModuleData();
        $infoModuleData->setShowLastUpdateTime(true);
        // Create wallet object.
        $wobObject = new Google_Service_Walletobjects_OfferObject();
        $wobObject->setClassId($issuerId.'.'.$classId);
        $wobObject->setId($issuerId.'.'.$objectId);
        $wobObject->setState('active');
        $wobObject->setVersion(1);
        $wobObject->setInfoModuleData($infoModuleData);      
        $wobObject->setLinksModuleData($linksModuleData);
        $wobObject->setKind('walletobjects#offerObject');
        return $wobObject;
    }
}
