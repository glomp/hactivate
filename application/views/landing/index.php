<div class="main-container">
    <p class="form-title">Welcome to h.activ8!</p>
    <p style="text-align:center; width: 100%">
        <img style="height: 50px; width: 250px" src="<?php echo base_url('assets/images/hactivate_logo.jpg') ?>" />
    </p>
    <p class="form-description">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
    </p>
    
    <form class="landing-form">
        <input name="name" type="text" placeholder="Name"/>
        <input name="email" type="text" placeholder="Email"/>
        <input name="mobile" type="text" placeholder="Mobile Number"/>
        <div class="button-container">
            <button type="button" onClick="createVoucher()">Submit</button>
        </div> 
    </form>
    <iframe id="downloadImage" style="display:none"></iframe>
</div>

<script>

    function createVoucher() {
          
        var empty = 0;
        $('input[type=text]').each(function(){
           if ($.trim(this.value) == "") {
               empty++;
               $("#error").show('slow');
           } 
        })
        
        if (empty <= 0) {
            var phone = getMobileOperatingSystem();   
            var form_data = $(".landing-form").serializeArray();

            form_data.push({name: 'phone', value: phone});
            console.log(form_data);
            $.post("customer/saveCustomer", form_data, function( data ) {
                var data = jQuery.parseJSON(data);
                alert(data.download);
                alert(data.success);
                $('#downloadImage').attr("src", data.download);

            });
            
            
            /**
            if (phone != "") {
                
            } else {
                alert("Available for android or iOS only");   
            }
            **/
        } else {
            alert("Fill up all text fields");
        }
          
    }
    
    
    function getMobileOperatingSystem() {
      var userAgent = navigator.userAgent || navigator.vendor || window.opera;

          // Windows Phone must come first because its UA also contains "Android"
        if (/windows phone/i.test(userAgent)) {
            return "";
        }

        if (/android/i.test(userAgent)) {
            return "Android";
        }

        // iOS detection from: http://stackoverflow.com/a/9039885/177710
        if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
            return "iOS";
        }

        return "";
    }
    
</script>

<?php
    
  /**
  
  {
	"style_keys": {
		"boardingPass": {
			"primaryFields": [{
				"key": "origin",
				"label": null,
				"value": "zzzzzzz"
			}, {
				"key": "destination",
				"label": null,
				"value": "yay"
			}],
			"secondaryFields": [{
				"key": "gate",
				"label": "Gate",
				"value": "F12"
			}, {
				"key": "date",
				"label": "Departure date",
				"value": "07\/11\/2012 10:22"
			}],
			"backFields": [{
				"key": "passenger-name",
				"label": "Passenger",
				"value": "John Appleseed"
			}],
			"transitType": "PKTransitTypeAir"
		}
	},
	"visual_appearance_keys": {
		"barcode": {
			"format": "PKBarcodeFormatQR",
			"message": "Flight-GateF12-ID6643679AH7B",
			"messageEncoding": "iso-8859-1"
		},
		"backgroundColor": "rgb(107,156,196)",
		"logoText": "Flight info"
	},
	"standard_keys": {
		"description": "Demo pass",
		"formatVersion": 1,
		"organizationName": "Flight Express",
		"serialNumber": "aaazmlkmiyyggssw123",
		"authenticationToken": "PZ8GL23T24JEB41X6U"
	}
}
  
  
  **/
  
?>
