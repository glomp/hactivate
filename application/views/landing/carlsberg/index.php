<?php 
  function current_full_url()
  {
      $CI =& get_instance();

      $url = $CI->config->base_url($CI->uri->uri_string());
      return $_SERVER['QUERY_STRING'] ? $url.'?'.$_SERVER['QUERY_STRING'] : $url;
  }

?>

  <body style="background: #061E1B !important">
    <div class="overlay" style="display:none;position: fixed;height: 100%;width: 100%;background-color: rgba(33, 31, 31, 0.5);top: 0px;z-index: 2;" ></div>

    <div style='display:none' id="download-ios">
         <iframe id="frame" src="" width="100%" height="300"></iframe>
    </div>


    <div id="overlay-2" style="display:none;padding: 10px;position: absolute;top: 123px;width: 100%;z-index: 3" class="row-fluid">
        <div style="background-color: #046d32;height: 208px;padding: 50px;" class="col s12">
            <div id="apple-btn" style="display:none;margin-top: 22px;" class="col s12">
                <div style="color:white;font-size: 18px;text-align: center;width: 100%;border: 1px solid white;border-radius: initial;font-weight: bold;" class="btn btn-flat">Apple</div>
            </div>
        </div>
    </div>

    <div id="download-note" style="display:none;margin-top: 30px;" class="row-fluid">
        <div style="color:white;padding: 30px; padding-top: 10px;" >
            <div class="row">
              <div class="col-md-12" align="center">
                <img style="width: 100%" class="img-responsive" src="<?php echo base_url('assets/images/carlsberg/outlets/banner.jpg'); ?>" >
              </div> 
            </div>

            <div style="margin-top: 10px;" class="row">
              <div align="center" class="col-md-12">
                  <span style="font-size: 14px;">Are you over 18?</span><br />
                  <span style="font-size: 14px;">你是否已年滿18歲?</span>
              </div>
            </div>
            <br />
            <div style="font-size: 10px;display: none;" class="error-msg alert alert-danger">
                <ul style="padding-left: 20px;">
                    <li>Thank you but this promotion is limited to those 18 and over.</li>
                </ul>
            </div>
            <form class="register-form" id="register-form">
                <div style="font-size: 11px;">
                  <label style="display: none"><input id="terms-and-conditions" value = "Yes" name="terms_and_conditions" type="checkbox"> Yes, I have read and agree to the 
                    <a style="color:white;" target ="_blank" href = "<?php echo base_url('landing/page/carlsberg/terms'); ?>" ><u>Terms & Conditions</u></a> of this voucher
                  </label>
                </div>
            </form>
            <div class="button-container">
                <div class="yes-btn" class="col s12">
                    <div style="color:white;font-size: 12px;text-align: center;width: 100%;border: 1px solid white;border-radius: initial;font-weight: bold;" class="btn btn-flat">Yes</div>
                </div>
                <br />
                <div class="no-btn" class="col s12">
                    <div style="color:white;font-size: 12px;text-align: center;width: 100%;border: 1px solid white;border-radius: initial;font-weight: bold;" class="btn btn-flat">No</div>
                </div>
            </div>
        </div>
    </div>

    <div id="banner" style="display:none;padding: 10px;position: absolute;top: 30px;width: 100%;z-index: 3" class="row-fluid">
        <div style="color:white; padding: 30px;padding-top: 0px;" >
            <div class="row">
              <div style = "padding: 2px;" class="col-xs-12">
                <img style="width: 100%" class="img-responsive" src="<?php echo base_url('assets/images/carlsberg/outlets/banner.jpg'); ?>" >
              </div>
            </div>
            <div style="margin-top: 50px;" class="button-container">
                <div class="next" class="col s12">
                    <div style="color:white;font-size: 12px;text-align: center;width: 100%;border: 1px solid white;border-radius: initial;font-weight: bold;" class="btn btn-flat">Next 下一步</div>
                </div>
            </div>
        </div>
    </div>


    <div id="download-ios-note-2" style="display:none;padding: 10px;position: absolute;top: 100px;width: 100%;z-index: 3" class="row-fluid">
        <div style="background-color: black; color:white; padding: 30px;border: 1px solid white" >
            <div class="col s12">To get the voucher, press “continue” to next page. Click “add” on top right corner to save it to your APPLE WALLET <img style="width: 10%" src="<?php echo base_url('assets/images/carlsberg/wallet.png'); ?>" ></div>
            <br />
            <div class="col s12">按 “繼續” 索取電子優惠劵。然後再按右上角 “加入” 儲存到 APPLE WALLET <img style="width: 10%" src="<?php echo base_url('assets/images/carlsberg/wallet.png'); ?>" ></div>
            <br />
            <div class="button-container">
                <div class="apple-note-btn-2" class="col s12">
                    <div style="color:white;font-size: 12px;text-align: center;width: 100%;border: 1px solid white;border-radius: initial;font-weight: bold;" class="btn btn-flat">Continue 繼續</div>
                </div>
            </div>
        </div>
    </div>

    <div id="download-android-note-2" style="display:none;padding: 10px;position: absolute;top: 100px;width: 100%;z-index: 3" class="row-fluid">
        <div style="background-color: black; color:white; padding: 30px;border: 1px solid white" >
            <div class="col s12">Your voucher will appear next. Please sign into your google account if necessary and click "Save" in order to save it to your Android Pay wallet. <img style="width: 10%" src="<?php echo base_url('assets/images/carlsberg/pay.png'); ?>" ></div>
            <br />
            <div class="col s12">電子優惠劵會於按下 “下一步” 後出現。請登入Google 帳戶後，按儲存，把優惠劵會儲存於你的 Android Pay Wallet。<img style="width: 10%" src="<?php echo base_url('assets/images/carlsberg/pay.png'); ?>" ></div>
            
            <div class="button-container">
                <div id="android-btn" style="margin-top: 22px;" class="col s12">
                    <div style="color:white;font-size: 12px;text-align: center;width: 100%;border: 1px solid white;border-radius: initial;font-weight: bold;" class="btn btn-flat">Next 下一步</div>
                </div>
                <div align="center" id="android-pay-btn" style="display:none;margin-top: 22px; border: 1px solid white;background: rgb(38, 51, 57); padding: 5px;" class="col s12"></div>            
            </div>
        </div>
    </div>


    <div id="share" style="display:none;padding: 10px;position: absolute;top: 100px;width: 100%;z-index: 3;text-align: justify;" class="row-fluid">
        <div style="color:white; padding: 30px;" >
            <div style="display: none;" id = "share-ios" class="col s12">DOWNLOADED! Please open your APPLE WALLET <img style="width: 10%" src="<?php echo base_url('assets/images/carlsberg/wallet.png'); ?>" > to use the coupon when you arrive at the bar/restaurant.
                <br /> <br />下載成功，請於到達酒吧/餐廳後打開APPLE WALLET <img style="width: 10%" src="<?php echo base_url('assets/images/carlsberg/wallet.png'); ?>" > 使用優惠劵!
            </div>
            <div style="display: none;" id = "share-android" class="col s12">DOWNLOADED! Please open your ANDROID PAY <img style="width: 10%" src="<?php echo base_url('assets/images/carlsberg/pay.png'); ?>" > to use the coupon when you arrive at the bar/restaurant.
                <br /> <br />下載成功，請於到達酒吧/餐廳後打開ANDROID PAY <img style="width: 10%" src="<?php echo base_url('assets/images/carlsberg/pay.png'); ?>" > 使用優惠劵!
            </div>

            <br />

            <div class="row" align="center"> 
              <div class="col-xs-12">Share with your friends <br /> 分享給你的朋友</div>
            </div>

            <div class="col s12" align="center">
                <!-- <a href="https://www.facebook.com/sharer/sharer.php?u=<?php //echo base_url('landing/page/carlsberg'); ?>" target="_blank">FB</a> -->
                <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo base_url('landing/page/carlsberg'); ?>" target="_blank">
                  <img style="width: 35px;" src = "<?php echo base_url('assets/images/carlsberg/fblogo.png') ?>" />
                </a>
                <a href="whatsapp://send?text=Buy 1 Get 1 free Carlsberg and selected food 50% off http://beerwithfood.me" data-action="share/whatsapp/share">
                  <img style="width: 35px;" src = "<?php echo base_url('assets/images/carlsberg/whatsapp.png') ?>" />
                </a>
                <a class = "hidden" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo base_url('landing/page/carlsberg?r=linkedin'); ?>" target="_blank">
                  <img style="width: 35px;" src = "<?php echo base_url('assets/images/carlsberg/linkedinlogo.png') ?>" />
                </a>
                <a class="hidden" href="http://twitter.com/share?text=The Body Shop The Body Shop The Body Shop The Body Shop The Body Shop The Body Shop The Body Shop The Body Shop&url=<?php echo base_url('landing/page/carlsberg?r=twitter'); ?>" target="_blank">
                  <img style="width: 35px;" src = "<?php echo base_url('assets/images/carlsberg/twitterlogo.png') ?>" />
                </a>
            </div>
        </div>
    </div>


    <div id="please-wait" style="display:none;padding: 10px;position: absolute;top: 60px;width: 100%;z-index: 3" class="row-fluid">
        <div style="color:white; padding: 50px;" >
            <div class="col s12">Please wait...</div>
        </div>
    </div>

    <div id="desktop-note" style="display:none;padding: 10px;position: absolute;top: 60px;width: 100%;z-index: 3" class="row-fluid">
        <div style="color:white; padding: 50px;" >
            <div class="col s12">In order to download the voucher, please go to http://beerwithfood.me on your mobile.</div>
        </div>
    </div>

    <div id="fb-note" style="display:none;padding: 10px;position: absolute;width: 100%;z-index: 3" class="row-fluid">
        <div style="color:white; padding: 50px;padding-top: 0px" >

            <div class="row">
              <div class="col-md-12" align="center">
                <img style="width: 100%" class="img-responsive" src="<?php echo base_url('assets/images/carlsberg/carlsberg-logo.jpg'); ?>" >
              </div> 
            </div>

            <div class="row">
                <div class="col col-xs-12" align="justify">
                Thank you for choosing to enjoy this promotion. The promotion's website can only be viewed in your default browser. Please click the button below to copy the voucher's website. Then in your default browser such as Safari or Chrome, please paste into your browser's address bar.
                <br /><br />
                Thank you.
                <br /><br />
                多謝你享用此優惠。此優惠網頁 只會顯示於你的預設瀏覽器。請 按下複製，然後把網址貼上至你 的預設瀏覽器。
                <br />
                <br />
                謝謝!
                <input id="fb-url" style="color: black" type="hidden" value="<?php echo base_url(uri_string()); ?>" />
                <br /><br />
                <div id="fb-btn" data-clipboard-text="<?php echo current_full_url(); ?>" style="color:white;font-size: 12px;text-align: center;width: 100%;border: 1px solid white;border-radius: initial;font-weight: bold;" class="btn btn-flat fb-btn">Copy</div>
              </div>
            </div>
        </div>
    </div>



  </body>


<script>
    var base_url = '<?php echo base_url(); ?>';
    var channel = '<?php echo (isset($_GET['channel'])) ? $_GET['channel'] : ''; ?>';
    var clipboard = new Clipboard('#fb-btn');

    clipboard.on('success', function(e) {
        $('#fb-btn').text('Copied!');
    });

    function isFacebookApp() {
        var ua = navigator.userAgent || navigator.vendor || window.opera;

        return (ua.indexOf("FBAN") > -1) || (ua.indexOf("FBAV") > -1);
    }

    if ( isFacebookApp()) {
        $('.overlay, #fb-note').toggle();
        throw new Error("Something went badly wrong!"); //just to finish execution
    }

    

    function download(form) {
      $('#please-wait').toggle(); //show
      if (localStorage.getItem('carlsberg_customer_id') === null) {
            //create new record
            $.ajax({
              method: "POST",
              url: '<?php echo base_url("client/carlsberg/dashboard/logs"); ?>',
              dataType: 'json',
              data: { 
                visit_id: localStorage.getItem('carlsberg_visit_id'),
                action: 'register',
                page: 'register-popup',
                data: $(form).serialize()
              },
              success : function(r) {
                if (r.visit_id > 0 ) {
                    $('#please-wait').toggle(); //hide
                    localStorage.setItem('carlsberg_customer_id', r.visit_id);

                    if (getMobileOperatingSystem() == 'iOS') {
                        $('#apple-btn').click();
                        return;
                    }

                    if (getMobileOperatingSystem() == 'Android') {
                        init();
                        return 
                    }
                    
                }
              }
            });
        } else {
            alert('Voucher already registered.');
        }
    }

    /**
     * Determine the mobile operating system.
     * This function returns one of 'iOS', 'Android', 'Windows Phone', or 'unknown'.
     *
     * @returns {String}
     */
    function getMobileOperatingSystem() {
      var userAgent = navigator.userAgent || navigator.vendor || window.opera;

          // Windows Phone must come first because its UA also contains "Android"
        if (/windows phone/i.test(userAgent)) {
            return "Windows Phone";
        }

        if (/android/i.test(userAgent)) {
            return "Android";
        }

        // iOS detection from: http://stackoverflow.com/a/9039885/177710
        if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
            return "iOS";
        }

        return "unknown";
    }


    function getUrlVars(url) {
        var hash;
        var myJson = {};
        var hashes = url.slice(url.indexOf('?') + 1).split('&');
        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            myJson[hash[0]] = hash[1];
        }
        return myJson;
    }

    if (getMobileOperatingSystem() == 'unknown') {
        $('.overlay, #desktop-note').toggle();
        throw new Error("Something went badly wrong!"); //just to finish execution
    }

    function limit_breaker() {
       var source = new EventSource('<?php echo base_url('iosVoucher/checkVoucherLimit/5'); ?>');

       source.addEventListener('message', function(e) {
        var data = JSON.parse(e.data);

        if (getMobileOperatingSystem() == 'Android') {
          if (data.android < 10000) {
              //no need to continue below alert limit
              return source.close();//close
          }

          if (data.android == 10000) {
                alert('Promo ended!');

                //$('.overlay, #download-android-note').toggle();

                source.close();
                return;
            }
        }

        if (getMobileOperatingSystem() == 'iOS') {
            if (data.ios < 10000) {
                //no need to continue below alert limit
                return source.close();//close
            }

            if (data.ios == 10000) {
                alert('Promo ended!');

                //$('.overlay, #download-ios-note').toggle();

                source.close();
                return;
            }
        }


      }, false);
    }

    //run voucher limit checker
    limit_breaker();


    /**
     * Save to android pay voucher success handler.
     */
    var successHandler = function (params) {
        //create new record
        $.ajax({
          method: "POST",
          url: '<?php echo base_url("client/carlsberg/dashboard/logs"); ?>',
          dataType: 'json',
          data: { 
            visit_id: localStorage.getItem('carlsberg_customer_id'),
            action: 'download',
            channel: 'android',
            page: 'landing/android'
          },
          beforeSend: function() {
            clicked = true;
          },
          success : function(r) {
            clicked = false; //open the function

            if (r.visit_id > 0 ) {
                $('.overlays, #download-android-note-2').toggle();
                showShareDialog();
                return false;
            } else {
                alert('Voucher already downloaded.');
            }
          }
        });
        
    }

    /**
     * Save to Wallet failure handler.
     */
    var failureHandler = function (params) {
        alert('Failed to save voucher');            
    }

    /**
     * Initialization function android pay
     */
    function init() {
        var loyaltyJwtUrl = base_url + 'androidvoucher/getcarlsbergjwt/' + localStorage.getItem('carlsberg_customer_id');
        
        $.when(
            // Get jwt of offer object and render 'Get offer' wallet button.
            $.get(loyaltyJwtUrl, function (data) {

              // gapi.savetoandroidpay.render("#android-pay-btn",{
              //   "jwt": data,
              //   "onsuccess": "successHandler",
              //   "onfailure": "failureHandler"
              // });

              saveToAndroidPay = document.createElement('g:savetoandroidpay');
              saveToAndroidPay.setAttribute('jwt', data);
              saveToAndroidPay.setAttribute('onsuccess', 'successHandler');
              saveToAndroidPay.setAttribute('onfailure', 'failureHandler');

              document.querySelector('#android-pay-btn').appendChild(saveToAndroidPay);
            })
        ).done(function () {
            $('#banner').toggle();
            //$('#android-btn').click();
            $(window).scrollTop(0);

            // It will execute after all above ajax requests are successful.
            script = document.createElement('script');
            script.src = 'https://apis.google.com/js/plusone.js';
            document.head.appendChild(script);
        });
    }

    function showShareDialog() {
        if (getMobileOperatingSystem() == 'iOS') {
            $('#share-ios').toggle();
        }

        if (getMobileOperatingSystem() == 'Android') {
            $('#share-android').toggle() 
        }

        $(window).scrollTop(0);
        $('.overlay, #share').toggle();
    }

    //Show Share
    if (localStorage.getItem('carlsberg_customer_id') !== null) {
        showShareDialog();
    } else {
        //show note
        if (localStorage.getItem('carlsberg_visit_id') !== null) {
            if (getMobileOperatingSystem() == 'iOS') {
                $('.overlays, #download-note').toggle();
            }

            if (getMobileOperatingSystem() == 'Android') {
                $('.overlays, #download-note').toggle(); 
            }
        }
    }

    //create new record for visitor
    if (localStorage.getItem('carlsberg_visit_id') === null) {
        //create new record
        $.ajax({
          method: "POST",
          url: '<?php echo base_url("client/carlsberg/dashboard/logs"); ?>',
          dataType: 'json',
          data: { 
            visit_id: 0,
            action: 'visitor',
            page: 'landing',
            from_channel: channel
          },
          success : function(r) {
            if (r.visit_id > 0 ) {
                localStorage.setItem('carlsberg_visit_id', r.visit_id);

                //show note
                if (getMobileOperatingSystem() == 'iOS') {
                    $('.overlays, #download-note').toggle(); 
                    return;
                }

                if (getMobileOperatingSystem() == 'Android') {
                    $('.overlays, #download-note').toggle(); 
                    return;
                }

            }
          }
        });   
    }

    $('#android-btn').click(function() {
        $(this).toggle();
        $('#android-pay-btn').toggle();
        return;
    });

    //show form
    $('.yes-btn, .no-btn').click(function(e) {
        e.preventDefault();

        var error_msg = $('#download-note').find('.error-msg')[0];
        var error_ul = $('#download-note').find('.error-msg > ul')[0];
        data = getUrlVars($('#register-form').serialize());
        //var terms = data.hasOwnProperty('terms_and_conditions'); //is checked
        var validate = true;
        var no = $(this).hasClass('no-btn');
        $(error_ul).html('') //clear
        $(error_msg).css('display', 'none'); //hide 

        //mandatory
        // if (! terms) {
        //     $(error_ul).append('<li>Please agree to the Terms & Conditions</li>');
        //     validate = false;
        // }

        if (no == true) {
            $(error_ul).append(
                '<li>Thank you but this promotion is limited to those 18 and over.</li> \
                 <li>多謝,但上述之優惠推動只適合年滿18歲或以上人士。</li>'
            );
            validate = false;
        }
        
        if (validate == false) {
            $(error_msg).css('display', 'block'); 
            window.scrollTo(0,$('#download-note').height()); //to bottom
            return; //show error message
        }

        if (getMobileOperatingSystem() == 'iOS') {
            return $('#download-note, #banner').toggle(); //show continue message
        }

        if (getMobileOperatingSystem() == 'Android') {
            $('#download-note').toggle(); //hide continue message
            return download('#register-form');
        }
    });

    //register then download
    $('.apple-note-btn-2').click(function() {
        $('.overlays, #download-ios-note-2').toggle();
        return download('#register-form');
    });

    //show download
    $('.next').click(function() {
        if (getMobileOperatingSystem() == 'iOS') {
            return $('.overlay, #download-ios-note-2, #banner').toggle();
        }

        if (getMobileOperatingSystem() == 'Android') {
            return $('.overlay, #download-android-note-2, #banner').toggle();
        }
    });

    //Download apple voucher
    $('#apple-btn').click(function() {
            var clicked = false;

            if (clicked == true) return; //do nothing

            //create new record
            $.ajax({
              method: "POST",
              url: '<?php echo base_url("client/carlsberg/dashboard/logs"); ?>',
              dataType: 'json',
              data: { 
                visit_id: localStorage.getItem('carlsberg_customer_id'),
                action: 'download',
                channel: 'ios',
                page: 'landing/ios'
              },
              beforeSend: function() {
                clicked = true;
              },
              success : function(r) {
                clicked = false; //open the function
                  

                if (r.visit_id > 0 ) {

                    //alert('Take note of customer ID for push notification:' + r.visit_id);
                    
                    //$('#frame').prop('src' , '<?php //echo base_url("IosVoucher/createVoucher/3/") ?>' + r.visit_id);

                    window.location = '<?php echo base_url("IosVoucher/createVoucher/5/") ?>' + r.visit_id;

                    setTimeout(function(){ 
                        showShareDialog();
                    }, 2000);

                    return false;
                } else {
                    alert('Voucher already downloaded.');
                }
              }
            });

    });
    //end ios functions

    //MISC
    $('#close-push-dialog').click(function() {
        $('.overlay, #overlay-3').toggle();
        return;
    });

        
</script>

