<!DOCTYPE html>
 <html>
 <head>
  <!-- Custom styles for this template -->
  <link href="<?php echo base_url('assets/css/style.css') ?>" rel="stylesheet">
  <link href="<?php echo base_url('assets/css/landing-page.css') ?>" rel="stylesheet">
  <link href="<?php echo base_url('assets/css/bootstrap.css') ?>" rel="stylesheet">

  <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400,300' rel='stylesheet' type='text/css'>


  <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
  <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

     
  <!--Let browser know website is optimized for mobile-->
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <meta name="format-detection" content="telephone=no" />
  <script src="<?php echo base_url('assets/js/jquery-2.0.3.min.js'); ?>"></script> 
  <script src="<?php echo base_url('assets/js/materialize/jquery.mobile.custom.js'); ?>"></script>
</head>

<style>
@font-face {
    font-family: 'century_gothicregular';
    src: url(<?php echo base_url('assets/images/issey-miyake/fonts/gothic-webfont.woff2'); ?>) format('woff2'),
         url(<?php echo base_url('assets/images/issey-miyake/fonts/gothic-webfont.woff'); ?>) format('woff');
    font-weight: normal;
    font-style: normal;
}

body {
  font-family: 'century_gothicregular';
}

</style>