
    <body class="landing-page landing-page1">
        
        <div class="wrapper">
             <div id ="home" class="parallax section-hactivate-gray" data-color="blue">
                 <div class= "container">
                    <div class="row">
                      
                        <div class="col-md-12" style="text-align: center">
                            <div class="description">
                             
                                <img style="width: 50%" src="<?php echo base_url('assets/images/hactivate-logo.png'); ?>">
                                <br>
                                <h5>an innovation in O2O marketing</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section section-hactivate-gray section-clients" style="border-top: 1px solid white;">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6" style="color:white; padding: 5%">
                            <h1 style="text-shadow: 0 1px 5px rgba(0,0,0,0.2);">People are <span style="color:red" >online.</span></h1>
                            <p>
                                ...more than in any other media channel. Whether its for communication, entertainment, or information, we are spending increasingly more time in digital media.<br>
                            </p>
                        </div>
                        <div align="right" class="col-md-6 hidden-xs">
                            <img style="width: 100%" src="<?php echo base_url('assets/images/onlinebanner.png'); ?>">
                        </div>
                        <div style="padding: 0px;" class="col-md-6 hidden-sm hidden-md hidden-lg">
                            <img style="width: 100%" src="<?php echo base_url('assets/images/onlinebanner.png'); ?>">
                        </div>
                    </div>
                    
                    
                </div>
            </div>
            <div class="section section-hactivate-gray section-clients" style="border-top: 1px solid white;">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6" style="color:white; padding: 5%">
                            <div class="description">
                                <h4 class="hidden header-text"></h4>
                                <p style="margin-top: 0px;">That is why we need to reach their customers online and on mobile.</p>
                                <p>For Brands which will provide their products and services offline, finding the right digital channels which not only communicate but effectively delivers customers is a challenge.</p>
                            
                            </div>
                        </div>
                        <div align="right" class="col-md-6">
                            <img style="width: 100%" src="<?php echo base_url('assets/images/page3-new.png'); ?>"/>

                        </div>
                    </div>
                </div>
            </div>

            <div class="section section-hactivate-gray section-clients" style="border-top: 0px solid white;">
                <div class="container">
                    <div class="row">
                        <div align="center" class="col-md-12">
                           
                            <a id="download" href="#" class="btn btn-default" style="" role="button">Demo</a>


                        </div>
                    </div>
                </div>
                <br><br>
            </div>

           <a href="https://www.pinterest.com/pin/create/button/" data-pin-do="buttonBookmark">
</a>
            
         </div>   

    </body>

    <div class="overlay" style="display: none; position: fixed;height: 100%;width: 100%;background-color: rgba(192, 192, 192, 0.7);top: 0px;z-index: 2" ></div>

    <div style='display:none' id="download-ios">
         <iframe id="frame" src="" width="100%" height="300"></iframe>
    </div>


    <div id="overlay-2" style="display:none;padding: 10px;position: absolute;top: 123px;width: 100%;z-index: 3" class="row-fluid">
        <div style="background-color: #505153;height: 208px;padding: 50px;" class="col s12">
            <div id="apple-btn" class="col s12 hidden">
                <div style="color:white;font-size: 18px;text-align: center;width: 100%;border: 1px solid white;border-radius: initial;font-weight: bold;" class="btn btn-flat">IPhone</div>
            </div>
            <div id="android-btn" style="margin-top: 22px;" class="col s12">
                <div style="color:white;font-size: 18px;text-align: center;width: 100%;border: 1px solid white;border-radius: initial;font-weight: bold;" class="btn btn-flat">Android</div>
            </div>
            <div align="center" id="android-pay-btn" style="display:none;margin-top: 22px;border: 1px solid white;background: rgb(73, 73, 73);" class="col s12"></div>
        </div>
    </div>

    <div id="overlay-3" style="display:none;padding: 10px;position: absolute;top: 123px;width: 100%;z-index: 3" class="row-fluid">
        <div style="background-color: #505153;padding: 50px;" class="col s12">
            <label style="color:white;">Test Push Notification by tapping "Test Push!" button</label>
          
            <a style="width: 100%" target="_blank" class="btn btn-default" id="test-push" >Test Push!</a>
            <br /><br />
            
            <button style="width: 100%" class="btn btn-default" id="close-push-dialog" >Cancel</button>
        </div>
    </div>


<script
    type="text/javascript"
    async defer
    src="//assets.pinterest.com/js/pinit.js"
></script>

<script>
    var base_url = '<?php echo base_url(); ?>';

    /**
     * Determine the mobile operating system.
     * This function returns one of 'iOS', 'Android', 'Windows Phone', or 'unknown'.
     *
     * @returns {String}
     */
    function getMobileOperatingSystem() {
      var userAgent = navigator.userAgent || navigator.vendor || window.opera;

          // Windows Phone must come first because its UA also contains "Android"
        if (/windows phone/i.test(userAgent)) {
            return "Windows Phone";
        }

        if (/android/i.test(userAgent)) {
            return "Android";
        }

        // iOS detection from: http://stackoverflow.com/a/9039885/177710
        if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
            return "iOS";
        }

        return "unknown";
    }



    /**
     * Save to android pay voucher success handler.
     */
    var successHandler = function (params) {
        //create new record
        $.ajax({
          method: "POST",
          url: '<?php echo base_url("client/demo/dashboard/logs"); ?>',
          dataType: 'json',
          data: { 
            visit_id: localStorage.getItem('demo_customer_id'),
            action: 'download',
            channel: 'android',
            page: 'landing/android'
          },
          beforeSend: function() {
            clicked = true;
          },
          success : function(r) {
            clicked = false; //open the function

            if (r.visit_id > 0 ) {
                $('#android-btn').toggle();
                $('#android-pay-btn').toggle();
                //do nothing
                return false;
            } else {
                alert('Voucher already downloaded.');
            }
          }
        });
        
    }

    /**
     * Save to Wallet failure handler.
     */
    var failureHandler = function (params) {
        alert('Failed to save voucher');            
    }

    /**
     * Initialization function android pay
     */
    function init() {
        var loyaltyJwtUrl = base_url + 'androidvoucher/getdemojwt/' + localStorage.getItem('demo_customer_id');
        
        $.when(
            // Get jwt of offer object and render 'Get offer' wallet button.
            $.get(loyaltyJwtUrl, function (data) {

              // gapi.savetoandroidpay.render("#android-pay-btn",{
              //   "jwt": data,
              //   "onsuccess": "successHandler",
              //   "onfailure": "failureHandler"
              // });

              saveToAndroidPay = document.createElement('g:savetoandroidpay');
              saveToAndroidPay.setAttribute('jwt', data);
              saveToAndroidPay.setAttribute('onsuccess', 'successHandler');
              saveToAndroidPay.setAttribute('onfailure', 'failureHandler');

              document.querySelector('#android-pay-btn').appendChild(saveToAndroidPay);
            })
        ).done(function () {
            $('.overlay, #overlay-2' ).toggle();
            $('#android-btn').click();
            $(window).scrollTop(0);

            // It will execute after all above ajax requests are successful.
            script = document.createElement('script');
            script.src = 'https://apis.google.com/js/plusone.js';
            document.head.appendChild(script);
        });
    }

    function showPushDialog() {
        $(window).scrollTop(0);
        $('.overlay, #overlay-3').toggle();
        $('#test-push').prop('href' , '<?php echo base_url('IosVoucher/message/'); ?>' + localStorage.getItem('demo_customer_id') );
    }

    //test push
    if (localStorage.getItem('demo_customer_id') !== null) {
        showPushDialog();
    }

    //create new record for visitor
    if (localStorage.getItem('demo_visit_id') === null) {
        //create new record
        $.ajax({
          method: "POST",
          url: '<?php echo base_url("client/demo/dashboard/logs"); ?>',
          dataType: 'json',
          data: { 
            visit_id: 0,
            action: 'visitor',
            page: 'landing'
          },
          success : function(r) {
            if (r.visit_id > 0 ) {
                localStorage.setItem('demo_visit_id', r.visit_id);
            }
          }
        });   
    }

    $('#android-btn').click(function() {
        $(this).toggle();
        $('#android-pay-btn').toggle();
        return;
    });

    $('#close-push-dialog').click(function() {
        $('.overlay, #overlay-3').toggle();
        return;
    });


    $('#apple-btn').click(function() {
            var clicked = false;

            if (clicked == true) return; //do nothing

            //create new record
            $.ajax({
              method: "POST",
              url: '<?php echo base_url("client/demo/dashboard/logs"); ?>',
              dataType: 'json',
              data: { 
                visit_id: localStorage.getItem('demo_customer_id'),
                action: 'download',
                channel: 'ios',
                page: 'landing/ios'
              },
              beforeSend: function() {
                clicked = true;
              },
              success : function(r) {
                clicked = false; //open the function
                  

                if (r.visit_id > 0 ) {

                    //alert('Take note of customer ID for push notification:' + r.visit_id);
                    
                    $('#frame').prop('src' , '<?php echo base_url("IosVoucher/createVoucher/9999/") ?>' + r.visit_id);

                    setTimeout(function(){ 
                      showPushDialog();
                    }, 2000);

                    return false;
                } else {
                    alert('Voucher already downloaded.');
                }
              }
            });

    });
    
    $('#download').click(function(e){
        e.preventDefault();

        if (localStorage.getItem('demo_customer_id') === null) {
            //create new record
            $.ajax({
              method: "POST",
              url: '<?php echo base_url("client/demo/dashboard/logs"); ?>',
              dataType: 'json',
              data: { 
                visit_id: localStorage.getItem('demo_visit_id'),
                action: 'register',
                page: 'register-popup',
                data: $('#register-form').serialize()
              },
              success : function(r) {
                if (r.visit_id > 0 ) {
                    localStorage.setItem('demo_customer_id', r.visit_id);

                    if (getMobileOperatingSystem() == 'iOS') {
                        $('#apple-btn').click();
                        return;
                    }

                    if (getMobileOperatingSystem() == 'Android') {
                        init();
                        return 
                    }
                    
                }
              }
            });
        } else {
            alert('Voucher already registered.');
        }

    });
</script>

