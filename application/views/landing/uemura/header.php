<!DOCTYPE html>
 <html>
 <head>
  <!-- Custom styles for this template -->
  <link href="<?php echo base_url('assets/css/style.css') ?>" rel="stylesheet">
  <link href="<?php echo base_url('assets/css/landing-page.css') ?>" rel="stylesheet">
  <link href="<?php echo base_url('assets/css/bootstrap.css') ?>" rel="stylesheet">

  <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400,300' rel='stylesheet' type='text/css'>


  <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
  <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

     
  <!--Let browser know website is optimized for mobile-->
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <meta name="format-detection" content="telephone=no" />
  <script src="<?php echo base_url('assets/js/jquery-2.0.3.min.js'); ?>"></script> 
  <script src="<?php echo base_url('assets/js/materialize/jquery.mobile.custom.js'); ?>"></script>
  <script src="<?php echo base_url('assets/js/clipboard/clipboard.js'); ?>"></script>

  <meta property="og:type" content="website" />
  <?php if (! isset($_GET['r'])) { ?>
  <meta property="og:title" content="Shu Uemura" />
  <meta property="og:description" content="立即前往植村秀位於海港城海運大廈入口大堂的 #mattitude pop-up store，領取專屬你的 #mattitude gift!" />
  <?php } ?>
  <meta property="og:image" content="<?php echo base_url('assets/images/uemura/strip.png'); ?>" />

  <?php if (isset($_GET['r'])) { ?>
    <?php if ($_GET['r'] == 'twitter') { ?>
         <meta property="og:title" content="Shu Uemura" />
         <meta property="og:description" content="立即前往植村秀位於海港城海運大廈入口大堂的 #mattitude pop-up store，領取專屬你的 #mattitude gift!" />
    <?php } ?>

    <?php if ($_GET['r'] == 'linkedin') { ?>
          <meta property="og:title" content="Shu Uemura" />
          <meta property="og:description" content="立即前往植村秀位於海港城海運大廈入口大堂的 #mattitude pop-up store，領取專屬你的 #mattitude gift!" />
    <?php } ?>
  <?php } ?>
</head>

<style>
@font-face {
  font-family: 'Conv_CarlsbergSans-Light';
  src: url(<?php echo base_url('/assets/images/carlsberg/fonts/CarlsbergSans-Light.eot') ?>);
  src: local('☺'), 
    url(<?php echo base_url('/assets/images/carlsberg/fonts/CarlsbergSans-Light.woff') ?>) format('woff'), 
    url(<?php echo base_url('assets/images/carlsberg/fonts/CarlsbergSans-Light.ttf'); ?>) format('truetype'),
    url(<?php echo base_url('assets/images/carlsberg/fonts/CarlsbergSans-Light.svg') ?>) format('svg');
  font-weight: normal;
  font-style: normal;
}

.btn.btn-square {
  border-radius: 0;
}

body {
  font-family: 'Conv_CarlsbergSans-Light';
}

</style>