<body style="background-color: #000000 !important;color:white;">

  <div class="container">

    <div class="row" style="margin-top:  50px;">
      <!-- <div class="col-md-12" align="center">
        <img style="width: 100%" class="img-responsive" src="<?php echo base_url('assets/images/cartier/prince-logo.png'); ?>" >
      </div> -->
      <div class="col-md-12" align="center">
        <img style="width: 100%" class="img-responsive" src="<?php echo base_url('assets/images/cartier/strip3.png'); ?>" >
      </div> 
    </div>

    <div class="row" style="margin-top:  5px;">
      <div class="col-xs-12" align="center">
        <button disabled id="redeem-btn" style="width: 100%;border: 1px white solid;border-radius: initial;color: gray;background-color: initial;font-size: 28px;margin-top: 60px;" class="btn btn_flat" type="submit">領取禮遇</button>
      </div> 
    </div>

    <div class="row" >
      <div class="col-xs-12" align="center">
        <button data-code="1" style="width: 100%;border: 1px white solid;border-radius: initial;color: white;background-color: initial;font-size: 28px;margin-top: 20px;" class="redeem btn btn_flat" type="submit">彌敦道總店</button>
      </div> 
    </div>

    <div class="row" >
      <div class="col-xs-12" align="center">
        <button data-code="2" style="width: 100%;border: 1px white solid;border-radius: initial;color: white;background-color: initial;font-size: 28px;margin-top: 20px;" class="redeem btn btn_flat" type="submit">太子集團中心分店</button>
      </div> 
    </div>

    <div class="row" >
      <div class="col-xs-12" align="center">
        <button data-code="3" style="width: 100%;border: 1px white solid;border-radius: initial;color: white;background-color: initial;font-size: 28px;margin-top: 20px;" class="redeem btn btn_flat" type="submit">海洋中心</button>
      </div> 
    </div>

    <div class="row" >
      <div class="col-xs-12" align="center">
        <button data-code="4" style="width: 100%;border: 1px white solid;border-radius: initial;color: white;background-color: initial;font-size: 28px;margin-top: 20px;" class="redeem btn btn_flat" type="submit">羅素街分店</button>
      </div> 
    </div>



  </div>

</body>
<script>
  var clicked = false;

   $('.redeem').click(function(e) {
        e.preventDefault();

        var r = confirm("You are about to redeem an item.");
        if (r == false) {
            return;
        }

        if (clicked == true) return; //do nothing

        var code = $(this).data('code');

        //create new record
        $.ajax({
          method: "POST",
          url: '<?php echo base_url("client/cartier/dashboard/redeem"); ?>',
          dataType: 'json',
          data: { 
            visit_id: '<?php echo $customer_id; ?>',
            action: 'redeem',
            page: 'redeem/confirm',
            code: code          
          },
          beforeSend: function() {
            clicked = true;
          },
          success : function(r) {
            clicked = false; //open the function

            if (r.success == true && r.outlet == true) {
                alert('Thank you.');
            } else if(r.outlet == false){
                alert('Wrong outlet code.');
            } else {
                alert('Already redeemed.');
            }
          }
        });
    });
</script>