<body style="font-family: 'century_gothicregular'; min-height: 500px;background-image: url('<?php echo base_url('assets/images/issey-miyake/im-bg.jpg') ?>'); background-size: 100%;background-color: rgb(231, 240, 235);background-repeat: no-repeat;">
    
    <div class="row" align="center"><img style ="margin-top: 27px;" class="responsive-img" src="<?php echo base_url('assets/images/issey-miyake/im-center-logo.png'); ?>" /></div>

    <div class="row" style ="padding: 0px 20px 0px 20px; margin-top: 80%;">
        <div class="row" >
            <div style="font-size: 13px;" class="col s4">櫃檯代碼:</div>
            <div class="col s4"><input style="width: 100%;border: solid 1px gray;" id="code" type="text" /></div>
        </div>

        <div style="margin-top: 5%;" >
            <div id="redeem-btn" style="border: 1px solid black;font-weight: bold;border-radius: initial;height: 31px; line-height: 28px;" class="btn btn-flat" >贖回</div>    
        </div>
    </div>
 </div>
    
</body>
</html>

<script>

   $('#redeem-btn').click(function() {
        var clicked = false;

        if (clicked == true) return; //do nothing

        //create new record
        $.ajax({
          method: "POST",
          url: '<?php echo base_url("client/issey/dashboard/redeem"); ?>',
          dataType: 'json',
          data: { 
            visit_id: '<?php echo $customer_id; ?>',
            action: 'redeem',
            page: 'redeem-hk/confirm',
            code: $('#code').val(),
          },
          beforeSend: function() {
            clicked = true;
          },
          success : function(r) {
            clicked = false; //open the function

            if (r.success == true && r.outlet == true) {
                alert('Thank you.');
            } else if(r.outlet == false){
                alert('Wrong outlet code.');
            } else {
                $('#code').val(''); //clear outlet code field
                alert('Already redeemed.');
            }
          }
        });
    });
</script>