<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta name='description' content='Wallet Object API Demo'>
    <title>Issey Miyake Demo</title>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.1/jquery.min.js">
    </script>
    <script>
        //TODO USER ID SHOULD BE PASSED DYNAMICALLY
        var userId = Math.floor(Math.random() * (100 - 1 + 1)) + 1;
        var baseURL = '<?php echo base_url()?>';
        var loyaltyJwtUrl = baseURL + 'androidvoucher/getloyaltyjwt?userid=' + userId;
        var demoJwtUrl = baseURL + 'androidvoucher/getdemojwt?userid=' + userId;

        /**
         * Save to Wallet success handler.
         */
        var successHandler = function (params) {
            console.log('Object added successfully' + params);
            
        }

        /**
         * Save to Wallet failure handler.
         */
        var failureHandler = function (params) {
            console.log('Object insertion failed' + params);
            
        }

        /**
         * Initialization function
         */
        function init() {
            $.when(
                // Get jwt of offer object and render 'Get offer' wallet button.
                $.get(loyaltyJwtUrl, function (data) {
                    saveToAndroidPay = document.createElement('g:savetoandroidpay');
                    saveToAndroidPay.setAttribute('jwt', data);
                    saveToAndroidPay.setAttribute('onsuccess', 'successHandler');
                    saveToAndroidPay.setAttribute('onfailure', 'failureHandler');
                    document.querySelector('#loyaltysave').appendChild(saveToAndroidPay);
                }),$.get(demoJwtUrl, function (data) {
                    saveToAndroidPay = document.createElement('g:savetoandroidpay');
                    saveToAndroidPay.setAttribute('jwt', data);
                    saveToAndroidPay.setAttribute('onsuccess', 'successHandler');
                    saveToAndroidPay.setAttribute('onfailure', 'failureHandler');
                    document.querySelector('#demosave').appendChild(saveToAndroidPay);
                })
            ).done(function () {
                // It will execute after all above ajax requests are successful.
                script = document.createElement('script');
                script.src = 'https://apis.google.com/js/plusone.js';
                document.head.appendChild(script);
            });
        }

        $(window).ready(function () {
            init();
        });
    </script>
</head>

<body>
    <h1>Voucher Demo</h1>
    <h4>Issey Miyake</h4>
    <div style="display:inline" id="loyaltysave"></div>
    <h4>h.@ctiv8</h4>
    <div style="display:inline" id="demosave"></div>
</body>

</html>