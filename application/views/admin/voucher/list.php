<?php $this->load->view('admin/global/header'); ?>

				<!-- page content -->
				<div class="right_col" role="main">

					<div class="row">
			           <a href="<?php echo site_url('admin/voucher/add_vouchers'); ?>" class="btn btn-primary btn-md">Add</a>  
			         </div>

			         <div class="row">
					<table id="example" class="display" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th>ID</th>
								<th>Customer Name</th>
								<th>Campaign Name</th>
								<th>Item</th>
								<th>Voucher Code</th>
								<th>Expiry Date</th>
								<th>Type</th>
								<th>Action</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th>ID</th>
								<th>Customer Name</th>
								<th>Campaign Name</th>
								<th>Item</th>
								<th>Voucher Code</th>
								<th>Expiry Date</th>
								<th>Type</th>
								<th>Action</th>
							</tr>
						</tfoot>
						<tbody>
							<?php foreach($campaign_vouchers as $campaign_voucher) { ?>
							<tr>
								<td><?php echo $campaign_voucher->id ?></td>
								<td><?php echo $campaign_voucher->customer_id ?></td>
								<td><?php echo $campaign_voucher->campaign_id ?></td>
								<td><?php echo $campaign_voucher->item_id ?></td>
								<td><?php echo $campaign_voucher->voucher_code ?></td>
								<td><?php echo $campaign_voucher->expiry_date ?></td>
								<td><?php echo $campaign_voucher->type ?></td>
								<td><a href="<?php echo site_url('admin/voucher/edit'); ?>"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span><a>
								</span><a></td>
							</tr>

							<?php } ?>
							
						</tbody>
					</table>

					 </div>

				</div>
				<!-- /page content -->

				<!-- footer content -->
				<?php $this->load->view('admin/global/footer'); ?>

		<!-- Custom Theme Scripts -->
		<script src="<?php echo base_url('assets/bower_components/datatables.net/js/jquery.dataTables.js'); ?>"></script>
		<script src="<?php echo base_url('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.js'); ?>"></script>

		<!-- Flot -->
		<script>
			$(document).ready(function() {
				 $('#example').DataTable();
			});
		</script>