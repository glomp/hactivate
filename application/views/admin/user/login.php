<!DOCTYPE html>
<html lang="en">
<head>

	<link href="<?php echo base_url('assets/admin/vendors/bootstrap/dist/css/bootstrap.min.css'); ?>" rel="stylesheet">
	<!-- Custom Theme Style -->
	<link href="<?php echo base_url('assets/admin/build/css/login.css'); ?>" rel="stylesheet">
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<div class="panel panel-login">
					<div class="panel-heading">
						<div class="row">
							<div class="col-lg-12 col-xs-12">
								<a href="#" class="active" id="login-form-link">Login</a>
							</div>
						</div>
						<hr>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-lg-12">
								<form id="login-form" action="<?php echo site_url('admin/login/in'); ?>" method="post" role="form" style="display: block;">
									<div class="form-group">
										<input type="text" name="username" id="username" tabindex="1" class="form-control" placeholder="E-mail" value="">
									</div>
									<div class="form-group">
										<input type="password" name="user_password" id="password" tabindex="2" class="form-control" placeholder="Password">
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-sm-6 col-sm-offset-3">
												<input type="submit" name="login-submit" id="login-submit" tabindex="4" class="form-control btn btn-login" value="Log In">
											</div>
										</div>
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-lg-12">
												<div class="text-center">
													<a href="http://phpoll.com/recover" tabindex="5" class="forgot-password">Forgot Password?</a>
												</div>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- jQuery -->
	<script src="<?php echo base_url('assets/admin/vendors/jquery/dist/jquery.min.js'); ?>"></script>
	<!-- Bootstrap -->
	<script src="<?php echo base_url('assets/admin/vendors/bootstrap/dist/js/bootstrap.min.js'); ?>"></script>

	<script type="text/javascript">
		$(document).ready(function() {

                $('#login-form').submit(function(e) {
                	e.preventDefault();
                	var form = this;

                	//start ajax
                    $.ajax({
                      method: "POST",
                      url: $(form).prop('action'),
                      data: $(form).serialize(),
                      dataType: 'json',
                      success : function(r) {
                      		//reset all first
                      		$(form).find('input').removeClass('error');
                      		$(form).find('input').tooltip('destroy');

                      		if (r.success == false) {
                      			//need to have delay to re-initialize tooltip
	                      		setTimeout(function(){ 
									$.each(r.messages, function( key, value ) {
										$('[name="' + key +'"]').addClass('error');
										$('[name="' + key +'"]').tooltip({
										  	title: value
										});
									});
								}, 300);
                      		} else {
                      			$('#header-message').removeClass('hidden');
                      			$('#header-message').html('Record has been successfully added.');
                      			$(form)[0].reset();
                      		}
                      }
                    });
                    //end ajax

                });
		});
	</script>
</body>
</html>