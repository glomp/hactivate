<?php $this->load->view('admin/global/header'); ?>
<!-- Custom Theme Style -->
<link href="<?php echo base_url('assets/admin/build/css/custom.css'); ?>" rel="stylesheet">

<!-- page content -->
<div class="right_col" role="main">

	<div class="">
		<div class="page-title">
			<div class="title_left">
				<h3>Clients</h3>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>Add Client</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
							</li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<br />
						<div id = "header-message" class="hidden alert alert-success"></div>
						<form id="user-form" data-parsley-validate class="form-horizontal form-label-left" action='<?php echo site_url('admin/client/add'); ?>'>

							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Name <span class="required">*</span>
								</label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<input type="text" name="name" id="name" class="form-control col-md-7 col-xs-12">
								</div>
							</div>

							
							<div class="ln_solid"></div>
							<div class="form-group">
								<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
									<a href="<?php echo site_url('admin/client'); ?>" class="btn btn-primary">Cancel</a>
									<button id='add-btn' type="submit" class="btn btn-success">Submit</button>
								</div>
							</div>

						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- /page content -->

		<!-- footer content -->
		<?php $this->load->view('admin/global/footer'); ?>

		<!-- start -->
		<script>
			$(document).ready(function() {

				$('#user_dob').daterangepicker({
					singleDatePicker: true,
				}, function(start, end, label) {
					console.log(start.toISOString(), end.toISOString(), label);
				});

				$('#user-form').submit(function(e) {
						e.preventDefault();
						var form = this;

	                	//start ajax
	                	$.ajax({
	                		method: "POST",
	                		url: $(this).prop('action'),
	                		data: $(this).serialize(),
	                		dataType: 'json',
	                		success : function(r) {
	                      		//reset all first
	                      		$(this).find('input').removeClass('error');
	                      		$(this).find('input').tooltip('destroy');

	                      		if (r.success == false) {
	                      			//need to have delay to re-initialize tooltip
	                      			setTimeout(function(){ 
	                      				$.each(r.messages, function( key, value ) {
	                      					$('[name="' + key +'"]').addClass('error');
	                      					$('[name="' + key +'"]').tooltip({
	                      						title: value
	                      					});
	                      				});
	                      			}, 300);
	                      		} else {
	                      			$('#header-message').removeClass('hidden');
	                      			$('#header-message').html('Record has been successfully added.');
	                      			$(this)[0].reset();
	                      		}
	                      	}
	                      });
	                    //end ajax

	                });
			});

			</script>
			<!-- /end -->