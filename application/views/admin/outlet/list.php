<?php $this->load->view('admin/global/header'); ?>

				<!-- page content -->
				<div class="right_col" role="main">

					<div class="row">
			           <a href="<?php echo site_url('admin/outlet/add'); ?>" class="btn btn-primary btn-md">Add</a>  
			         </div>

			         <div class="row">
					<table id="example" class="display" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th>Merchant ID</th>
								<th>Name</th>
								<th>Status</th>
								<th>Code</th>
								<th>Action</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th>Merchant ID</th>
								<th>Name</th>
								<th>Status</th>
								<th>Code</th>
								<th>Action</th>
							</tr>
						</tfoot>
						<tbody>
							<?php foreach($outlets as $outlet) { ?>
							<tr>
								<td><?php echo $outlet->id ?></td>
								<td><?php echo $outlet->name ?></td>
								<td><?php echo $outlet->status ?></td>
								<td><?php echo $outlet->code ?></td>
								<td><a href="<?php echo site_url('admin/outlet/edit'); ?>"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span><a>
								</span><a></td>
							</tr>

							<?php } ?>
							
						</tbody>
					</table>

					 </div>

				</div>
				<!-- /page content -->

				<!-- footer content -->
				<?php $this->load->view('admin/global/footer'); ?>

		<!-- Custom Theme Scripts -->
		<script src="<?php echo base_url('assets/bower_components/datatables.net/js/jquery.dataTables.js'); ?>"></script>
		<script src="<?php echo base_url('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.js'); ?>"></script>

		<!-- Flot -->
		<script>
			$(document).ready(function() {
				 $('#example').DataTable();
			});
		</script>