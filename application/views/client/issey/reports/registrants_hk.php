<?php $this->load->view('client/issey/header'); ?>

<!-- page content -->
<div class="right_col" role="main">
	<div class="row">
       <a id = "export-btn" href="<?php echo base_url('client/issey/reports/export_registrants_hk'); ?>" class="btn btn-primary btn-md">Export to Spreadsheet</a>  
     </div>
     <br />

     <div class="row">
		<table id="registrants" class="table" width="100%">
			<thead>
				<tr>
					<th>ID</th>
					<th>Info</th>
					<th>Date Time</th>
				</tr>
			</thead>
		</table>

	 </div>

</div>
<!-- /page content -->

	<!-- footer content -->
<footer>
  <div class="pull-right">
    h.@ctiv8 <?php echo date('Y'); ?>
  </div>
  <div class="clearfix"></div>
</footer>
<!-- /footer content -->

</div>
</div>

<!-- jQuery -->
<script src="<?php echo base_url('assets/admin/vendors/jquery/dist/jquery.min.js'); ?>"></script>
<!-- Bootstrap -->
<script src="<?php echo base_url('assets/admin/vendors/bootstrap/dist/js/bootstrap.min.js'); ?>"></script>
<!-- Custom Theme Scripts -->
<script src="<?php echo base_url('assets/bower_components/datatables.net/js/jquery.dataTables.js'); ?>"></script>
<script src="<?php echo base_url('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.js'); ?>"></script>
<script src="<?php echo base_url('assets/admin/vendors/moment/min/moment.min.js'); ?>"></script>

<!-- Custom Theme Scripts -->
<script src="<?php echo base_url('assets/admin/build/js/custom.min.js'); ?>"></script>

<!-- Bootstrap data tables -->
<link href="<?php echo base_url('assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css'); ?>" rel="stylesheet">

<!-- Flot -->
<script>
	$(document).ready(function() {
		 $('#registrants').DataTable( {
		    "processing": true,
		    "serverSide": true,
		    "paging": true,
		    "searching": false,
		    "order": [[ 2, 'desc' ]],
		    "ajax": {
		        "url": window.location,
		        "type": "POST"
		    },
		    "columnDefs": [ {
			    "targets": 2,
			    "render": function ( data, type, row ) {
			    	return moment(data).format('MMMM D, YYYY, h:mm:ss A');
		            //return data +' ('+ row[3]+')';
		        },
		  	} ],
		    "columns": [
		        { "data": "id", "orderable": false },
		        { "data": "info", "orderable": false },
		        { "data": "date_created" }
		    ]
		} );
	});
</script>